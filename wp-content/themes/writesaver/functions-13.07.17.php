<?php
/*
  Theme Name: writesaver
  Theme URI: 192.168.0.87/wp_content/themes/writesaver
  Description: A brief description.
  Version: 1.0
  Author: Adminforgot_pass
  Author URI: http://192.168.0.87/writesaver
 */

// writesaver_setup 

define('OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/inc/');
require_once dirname(__FILE__) . '/inc/options-framework.php';
global $wpdb;
date_default_timezone_set(get_option('timezone_string'));
if (!function_exists('writesaver_setup')) :

    function writesaver_setup() {
        add_theme_support('automatic-feed-links');
        add_theme_support('title-tag');
        add_theme_support('post-thumbnails');
        set_post_thumbnail_size(825, 510, true);
        flush_rewrite_rules();
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));

        add_theme_support('post-formats', array(
            'aside',
            'image',
            'video',
            'quote',
            'link',
            'gallery',
            'status',
            'audio',
            'chat',
        ));
    }

endif;
add_action('after_setup_theme', 'writesaver_setup');

//Register navigation menu
register_nav_menus(array(
    'header-menu' => __('Header Menu', 'writesaver'),
    'quick-link-menu' => __('Quick Links Menu', 'writesaver'), // Quick Links
    'customer-service-menu' => __('Customer Services Menu', 'writesaver'), //Customer Services
    'customer-dropdown-sub-menu' => __('Customer Dropdown Sub Menu', 'writesaver'), //Customer Dropdown Menu
    'proofreader-dropdown-sub-menu' => __('Proofreader Dropdown Sub Menu', 'writesaver'), //Proofreader Dropdown Menu
//'login_menu' => __('Login Menu', 'writesaver')
));




//-------Add words for new order -------//
add_filter('pmpro_after_checkout', 'add_words', 12, 4);

function add_words() {

    global $current_user, $wpdb, $pmpro_checkout_id;
    $user_id = $current_user->ID;
    $level_id = $_GET['level'];
    $order_detail = pmpro_getLevel($level_id);

    $plan_words = $order_detail->plan_words;
    $user_info = $wpdb->get_results("SELECT * FROM tbl_customer_general_info WHERE fk_customer_id = $user_id LIMIT 1");

    if (!empty($user_info)) {

        $remaining_credit_words = $user_info[0]->remaining_credit_words;
        $total_worls = $remaining_credit_words + $plan_words;
        $info_id = $user_info[0]->pk_customer_general_id;
        $wpdb->update(
                'tbl_customer_general_info', array('remaining_credit_words' => $total_worls), array('pk_customer_general_id' => $info_id), array('%d'), array('%d')
        );
    } else {
        $total_worls = $order_detail->plan_words;
    }

    $subject = 'You have new words on Writesaver.';
    $descs = 'You have new words in your Writesaver account! Our proofreaders are standing by and are ready to help you write with perfect, native English on your emails, papers, documents, proposals, and any other writing you can think of. If you have any questions about your account, feel free to shoot us an email at contact@writesaver.co, we\'ll get back to you as soon as we can, and we\'re always happy to help.


To your success in writing,
The Writesaver Team';
    $noti = 'You have new words available.';
    $descs .= '
<p>Below is some more information about your order:</p>
<p>Membership Level:' . $order_detail->name . '</p>
<p>Membership Fee: $' . $order_detail->initial_payment . '</p>
    <p>Words added: ' . $order_detail->plan_words . '</p>
    <p>Price Per Additional Word: ' . $order_detail->price_per_additional_word . '</p>
      <p>Total Words in your Account: ' . $total_worls . '</p>';

    send_cust_notification($user_id, '', $descs, 1, 1, $subject, $noti);
}

//custom_pagination

function custom_pagination($numpages = '', $pagerange = '', $paged = '') {

    if (empty($pagerange)) {
        $pagerange = 3;
    }
    global $paged;
    if (empty($paged)) {
        $paged = 1;
    }
    if ($numpages == '') {
        global $wp_query;
        $numpages = $wp_query->max_num_pages;
        if (!$numpages) {
            $numpages = 1;
        }
    }
    $pages = paginate_links(array(
        'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $numpages,
        'prev_next' => TRUE,
        'type' => 'array',
        'prev_next' => TRUE,
        'prev_text' => $PrevLink,
        'next_text' => $NextLink,
    ));
    $pagination_args = array(
        'base' => @add_query_arg('paged', '%#%'),
        'format' => 'page/%#%',
        'total' => $numpages,
        'current' => $paged,
        'show_all' => False,
        'end_size' => 1,
        'mid_size' => $pagerange,
        'prev_next' => True,
        'prev_text' => "Previous",
        'next_text' => "Next",
        'type' => 'array',
        'add_args' => false,
        'add_fragment' => ''
    );
    $paginate_links = paginate_links($pagination_args);

    if (is_array($paginate_links)) {
        $html = '';
        $paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
        $html .= '<ul class="pagination">';

        foreach ($paginate_links as $page) {
            $class_active = "";
            $temp_var = strip_tags($page);
            if ($temp_var == $paged) {
                $class_active = "active";
            }
            $html .= "<li class='$class_active'>$page</li>";
        }
        $html .= '</ul>';
        echo $html;
    }
}

//search filter    

function SearchFilter($query) {
    if ($query->is_search) {
        $query->set('post_type', 'post');
    }
}

add_filter('pre_get_posts', 'SearchFilter');

//custom_excerpt_length

function custom_excerpt_length($length) {
    return 25;
}

add_filter('excerpt_length', 'custom_excerpt_length', 999);

function custom_excerpt_more($more) {
    return ''; //you can change this to whatever you want
}

add_filter('excerpt_more', 'custom_excerpt_more');

// exclude pages from search        

add_action('pre_get_posts', 'exclude_all_pages_search');

function exclude_all_pages_search($query) {
    if (
            !is_admin() && $query->is_main_query() && $query->is_search && is_user_logged_in()
    )
        $query->set('post_type', 'post');
}

// custom post type for help center

function my_custom_post_help() {

    $labels = array(
        'name' => _x('Helps', 'post type general name'),
        'singular_name' => _x('Help', 'post type singular name'),
        'add_new' => _x('Add New', 'Help'),
        'add_new_item' => __('Add New Help'),
        'edit_item' => __('Edit Help'),
        'new_item' => __('New Help'),
        'all_items' => __('All Helps'),
        'view_item' => __('View Help'),
        'search_items' => __('Search helps'),
        'not_found' => __('No helps found'),
        'not_found_in_trash' => __('No helps found in the Trash'),
        'parent_item_colon' => '',
        'menu_name' => 'Helps'
    );
// args array
    $args = array(
        'labels' => $labels,
        'description' => 'Displays city helps and their ratings',
        'public' => true,
        'menu_position' => 4,
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'comments'),
        'has_archive' => true,
    );
    register_post_type('help', $args);
}

add_action('init', 'my_custom_post_help');

// custom categories for help center

function my_taxonomies_help() {
    $labels = array(
        'name' => _x('Help Categories', 'taxonomy general name'),
        'singular_name' => _x('Help  Category', 'taxonomy singular name'),
        'search_items' => __('Search Help Categories'),
        'all_items' => __('All Help Categories'),
        'parent_item' => __('Parent Help Category'),
        'parent_item_colon' => __('Parent Help Category:'),
        'edit_item' => __('Edit Help Category'),
        'update_item' => __('Update Help Category'),
        'add_new_item' => __('Add New Help Category'),
        'new_item_name' => __('New Help Category'),
        'menu_name' => __(' Help Categories'),
    );
    $args = array(
        'labels' => $labels,
        'hierarchical' => true,
    );
    register_taxonomy('help_category', 'help', $args);
}

add_action('init', 'my_taxonomies_help', 0);

function disable_admin_bar_for_subscribers() {
    if (is_user_logged_in()):
        global $current_user;
        if (!empty($current_user->caps['subscriber'])):
            add_filter('show_admin_bar', '__return_false');
        endif;
    endif;
}

add_action('init', 'disable_admin_bar_for_subscribers', 9);

//slider post type
function my_custom_post_proofreader() {

    $labels = array(
        'name' => _x('Proofreaders', 'post type general name'),
        'singular_name' => _x('Proofreader', 'post type singular name'),
        'add_new' => _x('Add New', 'proofreader'),
        'add_new_item' => __('Add New Proofreader'),
        'edit_item' => __('Edit Proofreader'),
        'new_item' => __('New Proofreader'),
        'all_items' => __('All Proofreaders'),
        'view_item' => __('View Proofreader'),
        'search_items' => __('Search Proofreaders'),
        'not_found' => __('No Proofreaders found'),
        'not_found_in_trash' => __('No Proofreaders found in the Trash'),
        'parent_item_colon' => '',
        'menu_name' => 'Proofreaders'
    );
// args array
    $args = array(
        'labels' => $labels,
        'description' => 'Displays city proofreaders and their ratings',
        'public' => true,
        'menu_position' => 4,
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'comments'),
        'has_archive' => true,
    );
    register_post_type('proofreader', $args);
}

add_action('init', 'my_custom_post_proofreader');

// shortcode for slider         

add_shortcode('proofreader', 'display_slider');

function display_slider() {

    $args = array(
        'post_type' => 'proofreader',
        'post_status' => 'publish',
        'post_per_page' => '-1'
    );
    $string = '';
    $string .= '<div class="become_main">';
    $string .= '<div class="become_slider_main">';
    $string .= '<div class="become_slider">';
    $string .= ' <div class="flexslider" id="become">';
    $query = new WP_Query($args);
    if ($query->have_posts()) {
        $string .= ' <ul class="slides">';
        while ($query->have_posts()) {
            $query->the_post();
            $feat_image = wp_get_attachment_url(get_post_thumbnail_id($query->ID));
            $string .= '<li>';
            $string .= ' <div class="become_slider_img">';
            $string .= ' <img src="' . $feat_image . '" class="img-responsive" alt="">';

            $string .= '    </div>';
            $string .= '<div class="become_main_block"> ';
            $string .= '<div class="become_main_inner_block">';
            $string .= ' <div class="become_main_top_title">';
            $string .= '<h2>' . get_the_title() . '</h2>';
            $string .= ' </div>';
            $string .= '<div class="become_main_top_txt">';
            $string .= '<p>';
            $string .= get_the_content();
            $string .= '</p>';
            $string .= ' </div>';
            $string .= ' </div>';
            $string .= ' </div>';
            $string .= ' </li>';
        }
        $string .= '</ul>';
    }
    $string .= ' </div>';
    $string .= '<div class="btn_blue">';
    $string .= '<a class="btn_sky" href="' . get_page_link(780) . '">Apply to Become a Proofreader</a>';
    $string .= '</div>';
    $string .= ' </div>';
    wp_reset_postdata();
    return $string;
// echo $string;
}

/* Ajax call for get state list store */
add_action('wp_ajax_getStates', 'getStates_callback');
add_action('wp_ajax_nopriv_getStates', 'getStates_callback');

function getStates_callback() {
    global $wpdb;
    $countryid = $_REQUEST['countryId'];
    $result_states = $wpdb->get_results("SELECT * from states where country_id=" . $countryid . "");

    $htmlData = "";

    foreach ($result_states as $value) {

        if ($value->name != "") :
            $htmlData .= '<option value="' . $value->id . '"> ' . $value->name . ' </option>';
        endif;
    }
    echo $htmlData;
    die();
}

add_action('wp_ajax_get_job', 'get_job_callback');
add_action('wp_ajax_nopriv_get_job', 'get_job_callback');

function get_job_callback() {
    global $wpdb;
    $user_ID = get_current_user_id();
    $jobs = $wpdb->get_results("SELECT * FROM job_history WHERE proof_id = $user_ID");
    $total_job = count($jobs);
    $job_html = '';

    $job_html .= ' <div class="job_parent">';

    if ($total_job > 0) {
        $count = 0;
        foreach ($jobs as $job) {
            $count++;

            $job_html .= ' <div class="job_history" id="job_' . $count . '">

                        <div class="col-sm-6">
                            <input  disabled="true" value="' . $job->company_name . '"  type="text" data-cname="' . $job->company_name . '" placeholder="company Name" class="job_info job_cname contact_block" id="cname' . $count . '" name="cname[]"   >
                        </div>
                        <div class="col-sm-6">
                            <input  disabled="true"  value="' . $job->designation . '" type="text" data-designation="' . $job->designation . '" placeholder="Designation" class="job_info job_des contact_block" id="designation' . $count . '"  name="designation[]" >
                        </div>
                        <div class="col-sm-6">
                            <input  disabled="true" value="' . date("d-m-Y", strtotime($job->start_date)) . '"   type="text" data-start_date="' . date("d-m-Y", strtotime($job->start_date)) . '" placeholder="Start date" class="job_date job_info job_sdate contact_block" id="sdate' . $count . '" name="sdate[]"   >
                        </div>
                        <div class="col-sm-5">
                            <input  disabled="true"  value="' . date("d-m-Y", strtotime($job->end_date)) . '"  type="text" data-end_date="' . date("d-m-Y", strtotime($job->end_date)) . '" placeholder="End date" class="job_date job_info job_edate contact_block" id="edate' . $count . '" name="edate[]"   >
                        </div>
                        <div class="col-sm-1">
                            <a style="display: none;"  class="del_job old_job delete" href="javascript:void(0);" data-job="' . $count . '"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                            <input type="hidden" class="job_id" name="job_id[]" value="' . $job->job_id . '"/>
                        </div>
                    </div>';
        }
    } else {
        $job_html .= ' <div class="job_history" id="job_1">
                    <div class="col-sm-6">
                        <input type="text" data-cname="" placeholder="company Name"  disabled="true" class="job_info job_cname contact_block" id="cname1" name="cname[]"   >
                    </div>
                    <div class="col-sm-6">
                        <input type="text" data-designation="" placeholder="Designation"  disabled="true" class="job_info job_des contact_block" id="designation1" name="designation[]" >
                    </div>
                    <div class="col-sm-6">
                        <input type="text" data-start_date="" placeholder="Start date"  disabled="true" class="job_date job_info job_sdate contact_block" id="sdate1" name="sdate[]" >
                    </div>
                    <div class="col-sm-6">
                        <input type="text" data-end_date="" placeholder="End date"  disabled="true" class="job_date job_info job_edate contact_block" id="edate1" name="edate[]">
                    </div>

                </div>';
    }
    $job_html .= ' </div>
        <div class="col-sm-6">
            <div class="fileinput fileinput-exists" data-provides="fileinput">
                <span class="btn btn-default btn-file"><span>Upload Resume</span>
                    <input type="hidden" value="" name="">                                                        
                    <input type="file" disabled="" name="upload_resume" id="upload_resume"  class="fileinput_info job_info" onchange="validateresumeFile(this.value)"/>
                </span>
                <span class="fileinput-filename">No file chosen</span>
                <span class="fileinput-new">No file chosen</span>
                <div class="pic_msg"></div>
            </div>';

    if (get_user_meta($user_ID, 'job_resume', true)) {
        $job_html .= '<div class="resume_div">';
        $url = get_user_meta($user_ID, 'job_resume', true);
        $job_html .= '<a href="' . $url . '" target="_blank">' . $name = basename($url) . '</a>';
        $job_html .= '<a href="javascript:void(0);" class="remove_resume" style="display:none;"><i class="fa fa-close" aria-hidden="true"></i> Remove</a>';
        $job_html .= '</div>';
    }

    $job_html .= '</div>';

    echo $job_html;
    die();
}

add_action('wp_ajax_save_jobhistory', 'save_jobhistory_callback');
add_action('wp_ajax_nopriv_save_jobhistory', 'save_jobhistory_callback');

function save_jobhistory_callback() {
    global $wpdb;
    $user_ID = get_current_user_id();
    $del_ids = $_POST['del_job'];
    $del_ids = explode(",", $del_ids);
    $job_count = $_POST['job_count'];
    for ($i = 0; $i <= $job_count; $i++) {

        $job_id = $_POST['job_id'][$i];
        if ($job_id) {
            if (in_array($job_id, $del_ids)) {
                $result = $wpdb->delete('job_history', array('job_id' => $job_id));
            } else {
                $result = $wpdb->update('job_history', array(
                    'proof_id' => $user_ID,
                    'company_name' => $_POST['cname'][$i],
                    'designation' => $_POST['designation'][$i],
                    'start_date' => date("Y-m-d", strtotime($_POST['sdate'][$i])),
                    'end_date' => date("Y-m-d", strtotime($_POST['edate'][$i]))
                        ), array('job_id' => $job_id));
            }
        } else {
            if ($_POST['cname'][$i] != '' || $_POST['designation'][$i] != '') {
                $result = $wpdb->insert('job_history', array(
                    'proof_id' => $user_ID,
                    'company_name' => $_POST['cname'][$i],
                    'designation' => $_POST['designation'][$i],
                    'start_date' => date("Y-m-d", strtotime($_POST['sdate'][$i])),
                    'end_date' => date("Y-m-d", strtotime($_POST['edate'][$i]))
                ));
            }
        }
    }
    if ($_POST['rem_resume'] == 1) {
        delete_user_meta($user_ID, 'job_resume');
    } else {
        $_FILES['upload_resume']['name'];
        if ($_FILES['upload_resume']['name']) {
            if (0 < $_FILES['file']['error']) {
                
            } else {
                $uploadFileName = $_FILES['upload_resume']['name'];
                if (isset($uploadFileName) && !empty($uploadFileName)) {

                    $upload_overrides = array('test_form' => FALSE);
                    $attach_image_file = '';
                    $attach_image_file = wp_handle_upload($_FILES['upload_resume'], $upload_overrides);
                    $fileurl = $attach_image_file['url'];

//Delete old profile form
                    $filename = get_user_meta($user_ID, 'job_resume', TRUE);
                    $urlparts = parse_url($filename);
                    $extracted = $urlparts['path'];
                    ltrim($extracted, '/');
                    $extracted = ABSPATH . $extracted;
                    if (file_exists($extracted)) {
                        unlink($extracted);
                    }

                    update_user_meta($user_ID, 'job_resume', $fileurl);
                    $data = array('resume' => $fileurl, 'message' => '<span class="text-success pic_msg1">Updated successfully...</span>');
                } else {
                    $data = array('resume' => '', 'message' => '<span class="text-danger pic_msg1">Profile not uploaded..</span>');
                }
            }
        }
    }
    echo '1';
    if ($_POST['info'])
        $update = update_user_meta($user_ID, 'info_completed', 1);
    die();
}

/* Ajax call for user card details store */
add_action('wp_ajax_user_cart_details_store', 'user_cart_details_store_callback');
add_action('wp_ajax_nopriv_user_cart_details_store', 'user_cart_details_store_callback');

function user_cart_details_store_callback() {
    global $wpdb;
    $user_id = get_current_user_id();

    $fname = $_REQUEST['firstname'];
    $lname = $_REQUEST['lastname'];
    $cardno = $_REQUEST['cardno'];
    $expdate = $_REQUEST['expdate'];
    $expyear = $_REQUEST['expyear'];
    $securitycode = $_REQUEST['securitycode'];
    $country = $_REQUEST['country'];
    $address = $_REQUEST['address'];
    $address1 = $_REQUEST['address1'];
    $city = $_REQUEST['city'];
    $state = $_REQUEST['state'];
    $zipcode = $_REQUEST['zipcode'];
    $phone = $_REQUEST['phone'];

    $prefix = $wpdb->prefix;
    $table_name = $prefix . 'creditdebit_card_details';


    $user = $wpdb->get_results("SELECT * FROM $table_name WHERE customer_id = $user_id");
    $user_count = count($user);
    if ($user_count > 0) {

        $insert = $wpdb->update(
                $table_name, array(
            'firstname' => $fname,
            'lastname' => $lname,
            'cardnumber' => $cardno,
            'expirymonth' => $expdate,
            'expyear' => $expyear,
            'securitycode' => $securitycode,
            'country' => $country,
            'address' => $address,
            'city' => $city,
            'address1' => $address1,
            'state' => $state,
            'zipcode' => $zipcode,
            'phone' => $phone), array('customer_id' => $user_id)
        );

        $insert = 1;
    } else {

        $insert = $wpdb->insert(
                $table_name, array(
            'customer_id' => $user_id,
            'firstname' => $fname,
            'lastname' => $lname,
            'cardnumber' => $cardno,
            'expirymonth' => $expdate,
            'expyear' => $expyear,
            'securitycode' => $securitycode,
            'country' => $country,
            'address' => $address,
            'city' => $city,
            'address1' => $address1,
            'state' => $state,
            'zipcode' => $zipcode,
            'phone' => $phone,
            'createddate' => date('Y-m-d')
                )
        );
    }
    echo $insert;
    die();
}

add_action('wp_ajax_nopriv_save_cust_paypal', 'save_cust_paypal_callback');
add_action('wp_ajax_save_cust_paypal', 'save_cust_paypal_callback');

function save_cust_paypal_callback() {
    global $wpdb;
    $user_id = get_current_user_id();
    $paypal_id = $_POST['paid_id'];

    $user = $wpdb->get_results("SELECT * FROM tbl_customer_general_info WHERE fk_customer_id = $user_id");
    $user_count = count($user);
    if ($user_count > 0) {
        $result = $wpdb->update('tbl_customer_general_info', array('paypal_id' => $paypal_id), array('fk_customer_id' => $user_id));
        $result = 1;
    } else {
        $result = $wpdb->insert('tbl_customer_general_info', array('fk_customer_id' => $user_id, 'paypal_id' => $paypal_id));
    }
    echo $result;
    die();
}

/* Ajax call for user registration */
add_action('wp_ajax_nopriv_user_signup', 'user_signup_callback');
add_action('wp_ajax_user_signup', 'user_signup_callback');

function user_signup_callback() {
    global $role;
    $fname = $_POST['fname'];
    $lname = $_POST['lname'];
    $email = $_POST['email'];
    $phone = $_POST['phone'];
    $pw = $_POST['pw'];
    $role = $_POST['role'];

    if (email_exists($email)) {
        echo '2';
    } else {
        global $wpdb;
        $prefix = $wpdb->prefix;
        $table_name = $prefix . 'pending_users';
        $datetime = date('Y-m-d H:i:s');
        $timestamp = strtotime($datetime);

        $wpdb->insert(
                $table_name, array(
            'fname' => $fname,
            'lname' => $lname,
            'email' => $email,
            'phone' => $phone,
            'password' => $pw,
            'role' => $role,
            'status' => 'active'
                )
        );

        if ($wpdb->insert_id):
            /* Email to user */
            $to = $email;

            $encrypt_method = "AES-256-CBC";
            $secret_key = 'This is my secret key';
            $secret_iv = 'This is my secret iv';

// hash
            $key = hash('sha256', $secret_key);

// iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
            $iv = substr(hash('sha256', $secret_iv), 0, 16);

            $output = openssl_encrypt($email, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);

            $subject = 'Your Writesaver Registration';
            $msg = '';
            $msg .= 'Thanks for registering for Writesaver! Please verify your email address by ';
            $msg .= '<a href="' . get_permalink(735) . '?string=' . $output . '">clicking here.</a>';


            $html = '<div style="width: 850px; margin: 0 auto">
            <div style="width: 250px; margin: 0 auto">
            <a style="margin: 0 15px 0px 0; width: 250px" href="' . get_site_url() . '" onclick="return false" rel="noreferrer">
                <img src="' . of_get_option('header_logo') . '" alt="logo" style="width: 100%">                
            </a>    
            </div>
        <div style="background: #f2f0f1; padding: 20px; border-radius: 15px; margin: 20px 0; display: inline-block; width: 100%">            
            <div style="width: 100%; display: inline-block; margin-bottom: 30px">
                <h2 style="color: #0071bd; font-size: 13px; text-transform: capitalize;">Hi ' . $fname . ' ' . $lname . ',</h2>
                <div >
                    <span style=" color: #333; margin: 0; width: 100%; display: inline-block; padding: 8px 5px">
                        ' . $msg . '
                    </span>
                </div>            
            </div>
            <div style="width: 100%; display: inline-block; margin-bottom: 30px">
                <h2 style="color: #0071bd; font-size: 13px;">To your success in writing,</h2>
                <div style="width: 100%; ">
                    <span style=" color: #333; margin: 0; width: 100%; display: inline-block; padding: 0">
                        ' . get_bloginfo() . '
                    </span>
                </div>
              </div>  
              <p style="font-size: 14px; color: #7c7c7c; line-height: 22px; text-align: center; ">' . of_get_option('copyright_text') . '</p>
        </div>
    </div>';
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            $headers .= 'From: ' . get_bloginfo() . ' <contact@writesaver.co>' . "\r\n";

            wp_mail($to, $subject, $html, $headers);

            echo '1';
        else:
            echo '0';
        endif;
    }

    die(0);
}

/* End ajax call for user registeration */
/* Ajax call for user signin */
add_action('wp_ajax_nopriv_user_signin', 'user_signin_callback');
add_action('wp_ajax_user_signin', 'user_signin_callback');

function user_signin_callback() {
    global $wpdb;
    $email = $_POST['email'];
    $pw = $_POST['pw'];



    if (isset($_POST['remember_me'])):
        $remember_me = 'true';
    else:
        $remember_me = 'false';
    endif;

    if ($email != "" && $pw != "") {
        $creds = array();
        $creds['user_login'] = $email;
        $creds['user_password'] = $pw;
        $creds['remember'] = $remember_me;

        $user = wp_signon($creds, false);

        if (is_wp_error($user)) {

            echo '0';
        } else {
            $user_id = $user->ID;
            $role = $user->roles[0];

            if ($role == 'customer') {
                echo get_the_permalink(762);
            } elseif ($role == 'proofreader') {
                $info = get_user_meta($user_id, 'info_completed', true);
                $test = get_user_meta($user_id, 'test_completed', true);
                if ($info == 1) {
                    if ($test == 1) {
                        echo get_the_permalink(810);
                    } else {
                        echo get_the_permalink(774);
                    }
                } else {
                    echo get_the_permalink(770);
                }
            } else {
                echo home_url();
            }
        }
    }

    die(0);
}

/* End ajax call for user signin */

//----------redirerction after login --------//
function redirect_login_page($redirect_to, $request, $user) {
    $user_id = $user->ID;
    if (isset($user->roles) && is_array($user->roles)) {
        if (in_array('customer', $user->roles)) {
            return get_the_permalink(762);
        } else if (in_array('proofreader', $user->roles)) {
            $info = get_user_meta($user_id, 'info_completed', true);
            $test = get_user_meta($user_id, 'test_completed', true);
            if ($info == 1) {
                if ($test == 1) {
                    return get_the_permalink(810);
                } else {
                    return get_the_permalink(774);
                }
            } else {
                return get_the_permalink(770);
            }
        } else {
            return $redirect_to;
        }
    }
}

add_filter('login_redirect', 'redirect_login_page', 10, 3);

/* Ajax call for Forgot Password */
add_action('wp_ajax_nopriv_forgot_pass', 'forgot_pass_callback');
add_action('wp_ajax_forgot_pass', 'forgot_pass_callback');

function forgot_pass_callback() {
    $email = $_POST['email'];


    if (email_exists($email)) {

        $to = $email;
        $user_info = get_user_by_email($email);
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'This is my secret key';
        $secret_iv = 'This is my secret iv';
// hash
        $key = hash('sha256', $secret_key);

// iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        $output = openssl_encrypt($email, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);

        $subject = 'Writesaver Password Reset';
        $msg .= 'We received a password reset request for your Writesaver account. Please reset your password by ';
        $msg .= '<a href=" ' . get_permalink(553) . '/?string=' . $output . '">clicking here.</a> If you did not request a password reset, please disregard this message.';

        $html = '<div style="width: 850px; margin: 0 auto">
            <div style="width: 250px; margin: 0 auto">
            <a style="margin: 0 15px 0px 0; width: 250px" href="' . get_site_url() . '" onclick="return false" rel="noreferrer">
                <img src="' . of_get_option('header_logo') . '" alt="logo" style="width: 100%">                
            </a>    
            </div>
        <div style="background: #f2f0f1; padding: 20px; border-radius: 15px; margin: 20px 0; display: inline-block; width: 100%">            
            <div style="width: 100%; display: inline-block; margin-bottom: 30px">
                <h2 style="color: #0071bd; font-size: 13px; text-transform: capitalize;">Hi ' . $user_info->first_name . ' ' . $user_info->last_name . ',</h2>
                <div >
                    <span style=" color: #333; margin: 0; width: 100%; display: inline-block; padding: 8px 5px">
                        ' . $msg . '
                    </span>
                </div>            
            </div>
            <div style="width: 100%; display: inline-block; margin-bottom: 30px">
                <h2 style="color: #0071bd; font-size: 13px;">Thank you!</h2>
                <div style="width: 100%; ">
                    <span style=" color: #333; margin: 0; width: 100%; display: inline-block; padding: 0">
                        The ' . get_bloginfo() . ' Team
                    </span>
                </div>
              </div>  
              <p style="font-size: 14px; color: #7c7c7c; line-height: 22px; text-align: center; ">' . of_get_option('copyright_text') . '</p>
        </div>
    </div>';
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        wp_mail($to, $subject, $html, $headers);

        echo '1';
    } else {
        echo '0';
    }
    die();
}

//ajax call for user upload document

add_action('wp_ajax_nopriv_user_upload_doc', 'user_upload_doc_callback');
add_action('wp_ajax_user_upload_doc', 'user_upload_doc_callback');

function user_upload_doc_callback() {


    if (0 < $_FILES['file']['error']) {
        echo 'Error: ' . $_FILES['file']['error'] . '<br>';
    } else {

        $file = $_FILES['file'];
        $fileName = $_FILES['file']['name'];
        $fileType = $_FILES['file']['type'];
        $fileTmp = $_FILES['file']['tmp_name'];
        $fileExtension = strtolower(end(explode('.', $_FILES['file']['name'])));

        if ($fileExtension == 'docx' || $fileExtension == 'doc' || $fileExtension == "txt") {
            $fileContent = read_docx($fileTmp, $fileExtension);
            //echo $fileContent;
            echo nl2br($fileContent);
        } elseif ($fileExtension == 'html') {
            echo $fileContent = file_get_contents($_FILES["file"]["tmp_name"]);
        } elseif ($fileExtension == 'odt') {
            echo (odt2html($fileTmp));
        } else {
            echo 'error';
        }
        die(0);
//   move_uploaded_file($_FILES['file']['tmp_name'], 'uploads/' . $_FILES['file']['name']);
    }
}

function read_docx($filename, $fileextension) {
    $striped_content = '';
    $content = '';
    if ($fileextension == "txt") {
        return file_get_contents($filename);
    } else if ($fileextension == "doc") {
        $fileHandle = fopen($filename, "r");
        $line = @fread($fileHandle, filesize($filename));
        $lines = explode(chr(0x0D), $line);
        $outtext = "";
        foreach ($lines as $thisline) {
            $pos = strpos($thisline, chr(0x00));
            if (($pos !== FALSE) || (strlen($thisline) == 0)) {

            } else {
                $outtext .= $thisline . " ";
                $outtext .= "\n";
            }
        }
        $outtext = preg_replace("/[^a-zA-Z0-9\s\,\.\-\t@\/\_\(\)]/", "", $outtext);
        return $outtext;
    } else {

        if (!$filename || !file_exists($filename)) {
            return false;
        }
        $zip = zip_open($filename);
        if (!$zip || is_numeric($zip)) {
            return false;
        }

        while ($zip_entry = zip_read($zip)) {
            if (zip_entry_open($zip, $zip_entry) == FALSE) {
                continue;
            }

            if (zip_entry_name($zip_entry) != "word/document.xml") {
                continue;
            }

            $content .= zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
            zip_entry_close($zip_entry);
        }
        zip_close($zip);
        $content = str_replace('</w:r></w:p></w:tc><w:tc>', " ", $content);
        $content = str_replace('</w:r></w:p>', "\r\n\r\n", $content);
        $striped_content = $content;
        //$striped_content = strip_tags($content);


        return $striped_content;
    }
}

//looks perfect update status single to complete

add_action('wp_ajax_nopriv_updateDocStatus', 'updateDocStatus_callback');
add_action('wp_ajax_updateDocStatus', 'updateDocStatus_callback');

function updateDocStatus_Callback() {
    global $wpdb;
    $double_doc_desc = $_POST['word_desc'];
    $proofreader_id = get_current_user_id();
    $datetime = date('Y-m-d H:i:s');
    $fk_cust_id = $_POST['fk_cust_id'];
    $fk_main_doc_id = $_POST['fk_main_doc_id'];
    $fk_sub_doc_id = $_POST['fk_sub_doc_id'];
    $user_role = $_POST['user_role'];
    $double_proof_id = $_POST['double_proof_id'];

    if ($user_role == 'admin') {
        if ($double_proof_id) {

            $double_proofreaders = $wpdb->get_row("SELECT * FROM `wp_assigned_document_details` WHERE fk_proofreader_id= $double_proof_id AND fk_doc_details_id =$fk_sub_doc_id");
            if ($double_proofreaders) {
                $wpdb->update('wp_assigned_document_details', array('fk_proofreader_id' => $proofreader_id, 'assign_date' => date('Y-m-d H:i:s')), array('fk_doc_details_id' => $fk_sub_doc_id, 'fk_proofreader_id' => $double_proof_id));
            } else {
                $wpdb->insert(
                        'wp_assigned_document_details', array(
                    'fk_doc_details_id' => $fk_sub_doc_id,
                    'fk_doc_main_id' => $fk_main_doc_id,
                    'fk_cust_id' => $fk_cust_id,
                    'fk_proofreader_id' => $proofreader_id,
                    'assign_date' => date('Y-m-d H:i:s'),
                    'status' => 'Completed',
                    'created_date' => date('Y-m-d H:i:s')
                        )
                );
            }
        } else {
            $wpdb->insert(
                    'wp_assigned_document_details', array(
                'fk_doc_details_id' => $fk_sub_doc_id,
                'fk_doc_main_id' => $fk_main_doc_id,
                'fk_cust_id' => $fk_cust_id,
                'fk_proofreader_id' => $proofreader_id,
                'assign_date' => date('Y-m-d H:i:s'),
                'status' => 'Completed',
                'created_date' => date('Y-m-d H:i:s')
                    )
            );
        }
        echo $wpdb->update(
                'tbl_proofreaded_doc_details', array(
            'status' => 'Completed',
            'DoubleCheckStatus' => '1',
            'Fk_DoubleProofReader_Id' => $proofreader_id
                ), array(
            'fk_doc_details_id' => $fk_sub_doc_id)
        );
    }

    echo $wpdb->update(
            'tbl_proofreaded_doc_details', array(
        'status' => 'Completed',
        'DoubleCheckStatus' => '1',
        'double_doc_desc' => $double_doc_desc
            ), array(
        'fk_doc_details_id' => $fk_sub_doc_id)
    );

    echo $wpdb->update(
            'wp_assigned_document_details', array(
        'status' => 'Completed',
        'modified_date' => $datetime,
        'completed_date' => $datetime
            ), array(
        'fk_doc_details_id' => $fk_sub_doc_id)
    );

    echo $wpdb->update(
            'wp_customer_document_details', array(
        'status' => 'Completed',
        'modified_date' => $datetime,
        'completed_date' => $datetime
            ), array(
        'pk_doc_details_id' => $fk_sub_doc_id)
    );


    $changed_wrod_ary = $_POST['changed_wrod_ary'];


    if ($user_role != 'admin') {

        $total_subdoc_count = $wpdb->get_row("SELECT doc_desc FROM `tbl_proofreaded_doc_details` WHERE fk_doc_details_id= $fk_sub_doc_id");
        $total_subdoc_count = get_word_count($total_subdoc_count->doc_desc);

        $total = count($changed_wrod_ary);
        $word_price = of_get_option('word_price_for_double_check');
        $amount = $word_price * $total_subdoc_count;
        $result = $wpdb->insert('tbl_proofreader_revenue', array(
            "fk_proofreader_id" => $proofreader_id,
            "fk_doc_id" => $fk_sub_doc_id,
            "requested_amount" => $amount,
            'edited_word' => $total,
            "status" => 'Pending',
            "created_date" => date('Y-m-d H:i:s')
        ));

        $revenues = $wpdb->get_results("SELECT * FROM `tbl_proofreader_revenue` WHERE fk_proofreader_id= $proofreader_id");
        if (count($revenues) > 0) {
            $total_amt = 0;
            $paid_amt = 0;
            $remaining_amt = 0;
            $pending_count = 0;
            foreach ($revenues as $revenue) {
                $total_amt = $total_amt + $revenue->requested_amount;
                $status = $revenue->status;
                if ($status == 'Pending')
                    $pending_count++;
                if ($status == 'Pending' || $status == 'Process') {
                    $remaining_amt = $remaining_amt + $revenue->requested_amount;
                }
                if ($status == 'Completed') {
                    $paid_amt = $paid_amt + $revenue->requested_amount;
                }
            }
        }

        $assign_documents = $wpdb->get_results("SELECT * FROM wp_assigned_document_details WHERE fk_proofreader_id= $proofreader_id AND status!='Pending' GROUP BY fk_doc_main_id DESC");
        $doc_count = count($assign_documents);

        $general_details = $wpdb->get_results("SELECT * FROM tbl_proofreader_general_details WHERE fk_proofreader_id = $proofreader_id LIMIT 1");
        if ($general_details) {
            $word_count = $general_details[0]->total_words_edited + $total;
            $result = $wpdb->update('tbl_proofreader_general_details', array(
                'total_docs_worked' => $doc_count,
                'total_words_edited' => $word_count,
                'total_earning' => $total_amt,
                'total_paid_amount' => $paid_amt,
                'remaining_amount' => $remaining_amt
                    ), array('fk_proofreader_id' => $proofreader_id)
            );
        } else {
            $word_count = $total;
            $result = $wpdb->insert('tbl_proofreader_general_details', array(
                'fk_proofreader_id' => $proofreader_id,
                'total_docs_worked' => $doc_count,
                'total_words_edited' => $word_count,
                'total_earning' => $total_amt,
                'total_paid_amount' => $paid_amt,
                'remaining_amount' => $remaining_amt
                    )
            );
        }

        $main_doc = $wpdb->get_results("SELECT * FROM `wp_customer_document_main`  Where pk_document_id =  $fk_main_doc_id AND Status =1 LIMIT 1 ");
        $main_docName = $main_doc[0]->document_title;
        $cust_id = $main_doc[0]->fk_customer_id;
        $cust_info = get_userdata($cust_id);
        $result = $wpdb->insert('tbl_proofreader_history', array(
            "fk_proofreader_id" => $proofreader_id,
            "date" => date('Y-m-d H:i:s'),
            'doc_name' => $main_docName,
            "customer" => $cust_info->first_name . ' ' . $cust_info->last_name,
            'total_words' => $total_subdoc_count,
            "edited_words" => $total,
            "earned" => $amount,
            "created_date" => date('Y-m-d H:i:s')
        ));
    }

    foreach ($changed_wrod_ary as $value) {

        $result = $wpdb->insert('tbl_document_word_tracking', array(
            "wrong_word" => $value[2],
            "wrong_start_offset" => 0,
            "wrong_end_offset" => 0,
            "wrong_length" => $value[0],
            "fk_main_doc_id" => $fk_main_doc_id,
            "fk_subdoc_id" => $fk_sub_doc_id,
            "fk_proofreader_id" => $proofreader_id,
            "corrected_word" => $value[2],
            "corrected_word_start_offset" => 0,
            "corrected_word_end_offset" => 0,
            "corrected_word_length" => $value[0],
            "Action" => $value[1],
            "createddate" => $datetime,
            "modifieddate" => $datetime,
            "check_status" => "Double Check"
        ));
    }

    $main_doc = $wpdb->get_row("SELECT * FROM wp_customer_document_details where pk_doc_details_id= $fk_sub_doc_id ");
    $result_maindoc_name = $wpdb->get_var("SELECT document_title FROM wp_customer_document_main WHERE pk_document_id=$main_doc->fk_doc_main_id AND Status=1");
    $total_docs = $wpdb->get_results("SELECT * FROM wp_customer_document_details WHERE fk_doc_main_id= $main_doc->fk_doc_main_id AND is_active= 1 ORDER BY pk_doc_details_id");
    $total_count = count($total_docs);
    $count = 0;
    $complete_count = 0;
    foreach ($total_docs as $total_doc) {
        $count++;
        if ($total_doc->pk_doc_details_id == $fk_sub_doc_id) {
            $doc_count = $count;
        }
        if ($total_doc->status == "Completed") {
            $complete_count++;
        }
    }


    if ($complete_count == $total_count) {
        
         $wpdb->update(
                'wp_customer_document_main', array(
            'Completed_date' => $datetime,
                ), array(
            'pk_document_id' => $main_doc->fk_doc_main_id)
        );

        $user_ID = $main_doc->fk_cust_id;
        $proof_info = get_userdata($proofreader_id);
        $subject =  "Your Writesaver Proofread is Complete!";
        $desc = "<p>We've finished proofreading ". $result_maindoc_name ." for you!</p>";
        $noti = "<p>We've finished proofreading " . $result_maindoc_name . ".</p>";

        $desc .= 'You can check it out by ';
        $desc .= '<a href=" ' . get_permalink(924) . '/?doc=' . $main_doc->fk_doc_main_id . '">clicking here.</a><br/><br/> ';
        $desc .= "If you have any questions or feedback about your proofread, just shoot us a reply to this message and we'll get back to you right away.";

        $result_user = $wpdb->get_results("SELECT * FROM wp_notification_settings WHERE user_id= $user_ID");
        $desk_noti = $result_user[0]->dash_doc_completed;
        $email_noti = $result_user[0]->receive_doc_completed;

        send_cust_notification($user_ID, $proofreader_id, $desc, $desk_noti, $email_noti, $subject, $noti);
    }
    die(0);
}

add_action('wp_ajax_nopriv_getDoubleCheckDesc', 'getDoubleCheckDesc_callback');
add_action('wp_ajax_getDoubleCheckDesc', 'getDoubleCheckDesc_callback');

function getDoubleCheckDesc_Callback() {
    global $wpdb;
    $proofreader_id = get_current_user_id();
    $result = $wpdb->get_row("SELECT * FROM tbl_proofreaded_doc_details where  Fk_DoubleProofReader_Id= $proofreader_id and DoubleCheckStatus=0");
    if (count($result) > 0) {
        echo json_encode($result);
    } else {
        echo '0';
    }
    die(0);
}

add_action('wp_ajax_nopriv_getDoubleCheckDoc', 'getDoubleCheckDoc_callback');
add_action('wp_ajax_getDoubleCheckDoc', 'getDoubleCheckDoc_callback');

function getDoubleCheckDoc_callback() {
    global $wpdb;
    $proofreader_id = get_current_user_id();
    $mainDocId = $_POST['mainDocId'];
    $detailDocId = $_POST['detailDocId'];
    $result = $wpdb->get_row("SELECT * FROM wp_customer_document_main where pk_document_id= $mainDocId AND Status =1");


    $result_doc = $wpdb->get_results(" SELECT * FROM wp_customer_document_details AS D "
            . " INNER JOIN wp_customer_document_main AS M"
            . " ON D.fk_doc_main_id = M.pk_document_id"
            . " where D.fk_doc_main_id= $mainDocId AND D.pk_doc_details_id =$detailDocId"
            . " and D.is_active=1 AND M.Status =1 ORDER BY M.order_no ASC LIMIT 1");
    if (count($result) > 0) {
        $docTitle = $result->document_title;
        $doubleDocHTML = '';
        $doubleDocHTML .= ' <div class="parentscrollcontents" style="width: 100%; height:300px; display: inline-block;">';


        $all_doc = $wpdb->get_results("SELECT * FROM wp_customer_document_details WHERE fk_doc_main_id= $mainDocId AND is_active = 1");

        foreach ($all_doc as $doc) {
            if ($doc->pk_doc_details_id == $detailDocId) {
                $contenteditable = 'TRUE';
                $div_id = 'txt_area_upload_doc';
                $color = 'white';
            } else {
                $contenteditable = 'FALSE';
                $div_id = 'txt_area_upload_doc_' . $doc->pk_doc_details_id;
                $color = '#f5f5f5';
            }
            $doubleDocHTML .= '<div class="changeable check " contenteditable="' . $contenteditable . '" id="' . $div_id . '" data-id="' . $doc->pk_doc_details_id . '" style="background-color: ' . $color . '" >';

            $status_1 = $doc->status;
            if ($status_1 == "Pending" || $status_1 == "In Process") {
                $doubleDocHTML .= stripslashes(str_replace("\n", "<br>", trim($doc->document_desc)));
            } else {
                $docdtl_Id = $doc->pk_doc_details_id;
                $Proofreaded_doc1 = $wpdb->get_row("SELECT * FROM tbl_proofreaded_doc_details where  fk_doc_details_id =$docdtl_Id ");
                $doubleDocHTML .= stripslashes(str_replace("\n", "<br>", trim($Proofreaded_doc1->doc_desc)));
            }
            $doubleDocHTML .= '     </div>';
        }
        $doubleDocHTML .= '  </div>';
        $doubleDocHTML .= '<div id="original" style="white-space: pre-line;position: relative;display:none;height:500px; width: 100%;white-space: pre-line; display:none;">
                            ' . stripslashes(trim($result_doc[0]->document_desc)) . ' 
                        </div>
                        <div id="temdoc" style="white-space: pre-line;position: relative;display:none;"> ' . stripslashes(trim($result_doc[0]->document_desc)) . ' </div>
                        <div id="deletedwords" style="display: none;"></div>
                        <div id="deletedwordsbackspace" style="display: none;"></div>
                        <input type="hidden" id="hdndocidpartsid" name="hdndocidpartsid" value="' . $detailDocId . '" />
                        <input type="hidden" id="fk_proofreader_id" name="fk_proofreader_id" value="' . $result->fk_proofreader_id . '" />';

        echo json_encode(array('title' => $docTitle, 'html' => $doubleDocHTML));
    } else {
        echo '0';
    }
    die(0);
}

//update status when click finish my section
add_action('wp_ajax_nopriv_submited_doc_by_proofreader', 'submited_doc_by_proofreader_callback');
add_action('wp_ajax_submited_doc_by_proofreader', 'submited_doc_by_proofreader_callback');

function submited_doc_by_proofreader_callback() {

    global $wpdb;
    $desc = $_POST['word_desc'];
    $proofreader_id = get_current_user_id();
    $datetime = date('Y-m-d H:i:s');
    $fk_cust_id = $_POST['fk_cust_id'];
    $fk_main_doc_id = $_POST['fk_main_doc_id'];
    $fk_sub_doc_id = $_POST['fk_sub_doc_id'];
    $user_role = $_POST['user_role'];

    if ($user_role == 'admin') {
        $documents = $wpdb->get_row("SELECT * FROM `wp_customer_document_details` WHERE pk_doc_details_id= $fk_sub_doc_id");
        if ($documents) {
            $status = $documents->status;
            if ($status == 'Pending') {
                $result_maindoc_name = $wpdb->get_var("SELECT document_title FROM wp_customer_document_main WHERE pk_document_id=$fk_main_doc_id AND Status=1");
                $total_docs = $wpdb->get_results("SELECT * FROM wp_customer_document_details WHERE fk_doc_main_id= $fk_main_doc_id AND is_active= 1 ORDER BY pk_doc_details_id");
                $count = 0;
                $pending_count = 0;
                $total_count = count($total_docs);
                foreach ($total_docs as $total_doc) {
                    $count++;
                    if ($total_doc->pk_doc_details_id == $fk_sub_doc_id) {
                        $doc_count = $count;
                    }
                    if ($total_doc->status == "Pending") {
                        $pending_count++;
                    }
                }
                $proof_info = get_userdata($proofreader_id);
                $subject = "We've started editing ". $result_maindoc_name;
                $mail_desc = "<p>Our proofreaders have started editing " . $result_maindoc_name . ". It'll be ready for you soon!</p>";
                $noti = "<p>We have started editing " . $result_maindoc_name . ".</p>";

                $result_user = $wpdb->get_results("SELECT * FROM wp_notification_settings WHERE user_id= $fk_cust_id");
                $desk_noti = $result_user[0]->dash_doc_started;
                $email_noti = $result_user[0]->receive_doc_started;
                if ($pending_count == $total_count) {
                    echo send_cust_notification($fk_cust_id, $proofreader_id, $mail_desc, $desk_noti, $email_noti, $subject, $noti);
                }
            }

            $assign = $wpdb->get_row("SELECT * FROM `wp_assigned_document_details` WHERE fk_doc_details_id= $fk_sub_doc_id");
            if ($assign) {
                $wpdb->update('wp_assigned_document_details', array('fk_proofreader_id' => $proofreader_id, 'assign_date' => date('Y-m-d H:i:s')), array('fk_doc_details_id' => $fk_sub_doc_id));
            } else {
                $wpdb->insert(
                        'wp_assigned_document_details', array(
                    'fk_doc_details_id' => $fk_sub_doc_id,
                    'fk_doc_main_id' => $fk_main_doc_id,
                    'fk_cust_id' => $fk_cust_id,
                    'fk_proofreader_id' => $proofreader_id,
                    'assign_date' => date('Y-m-d H:i:s'),
                    'status' => 'Single Check',
                    'created_date' => date('Y-m-d H:i:s')
                        )
                );
            }
        }
    }


//    $result_proofreaded_doc = $wpdb->get_results("Select * from tbl_proofreaded_doc_details where Fk_DoubleProofReader_Id=$proofreader_id and DoubleCheckStatus=1 and status='Single Check'");
//    if (count($result_proofreaded_doc) == 0) {
    $result = $wpdb->get_results("SELECT * from wp_users join wp_usermeta on wp_users.ID = wp_usermeta.user_id where wp_users.ID != $proofreader_id AND wp_users.ID NOT IN(select Fk_DoubleProofReader_Id from tbl_proofreaded_doc_details where DoubleCheckStatus= 0 AND Fk_DoubleProofReader_Id != $proofreader_id) AND wp_usermeta.meta_key='role' and wp_usermeta.meta_value='proofreader'  AND wp_usermeta.meta_key='role' and wp_usermeta.meta_value='proofreader'  ");

    $DoubleProofReader_Id = NULL;
    //if (count($result) > 0)
    //    $DoubleProofReader_Id = $result[0]->ID;
    //else
    //    $DoubleProofReader_Id = NULL;
    $wpdb->insert(
            'tbl_proofreaded_doc_details', array(
        'fk_cust_id' => $fk_cust_id,
        'fk_doc_main_id' => $fk_main_doc_id,
        'fk_doc_details_id' => $fk_sub_doc_id,
        'fk_proofreader_id' => $proofreader_id,
        'Fk_DoubleProofReader_Id' => $DoubleProofReader_Id,
        'doc_desc' => $desc,
        'status' => 'Single Check',
        'createddate' => $datetime,
        'DoubleCheckStatus' => FALSE
            )
    );

    $wpdb->update(
            'wp_assigned_document_details', array(
        'status' => 'Single Check',
        'modified_date' => $datetime,
        
            ), array(
        'fk_doc_details_id' => $fk_sub_doc_id)
    );

    $wpdb->update(
            'wp_customer_document_details', array(
        'status' => 'Single Check',
        'modified_date' => $datetime,
            ), array(
        'pk_doc_details_id' => $fk_sub_doc_id)
    );
//    }
//word tracking details insert.
    $user_id = get_current_user_id();
    $doc_id = $_POST['doc_id'];
    $result = "";
    $data = $_POST['changed_wrod_ary'];

    if ($user_role != 'admin') {
        $total_subdoc_count = $wpdb->get_row("SELECT * FROM `wp_customer_document_details` WHERE pk_doc_details_id= $fk_sub_doc_id");
        $total = count($data);
        $total_subdoc_count = $total_subdoc_count->word_end_no - $total_subdoc_count->word_start_no;
        $word_price = of_get_option('word_price_for_single_check');
        $amount = $word_price * $total_subdoc_count;
        $result = $wpdb->insert('tbl_proofreader_revenue', array(
            "fk_proofreader_id" => $proofreader_id,
            "fk_doc_id" => $doc_id,
            "requested_amount" => $amount,
            'edited_word' => $total,
            "status" => 'Pending',
            "created_date" => date('Y-m-d H:i:s')
        ));

        $revenues = $wpdb->get_results("SELECT * FROM `tbl_proofreader_revenue` WHERE fk_proofreader_id= $proofreader_id");
        if (count($revenues) > 0) {
            $total_amt = 0;
            $paid_amt = 0;
            $remaining_amt = 0;
            $pending_count = 0;
            foreach ($revenues as $revenue) {
                $total_amt = $total_amt + $revenue->requested_amount;
                $status = $revenue->status;
                if ($status == 'Pending')
                    $pending_count++;
                if ($status == 'Pending' || $status == 'Process') {
                    $remaining_amt = $remaining_amt + $revenue->requested_amount;
                }
                if ($status == 'Completed') {
                    $paid_amt = $paid_amt + $revenue->requested_amount;
                }
            }
        }

        $assign_documents = $wpdb->get_results("SELECT * FROM wp_assigned_document_details WHERE fk_proofreader_id= $proofreader_id AND status!='Pending' ");
        $doc_count = count($assign_documents);

        $general_details = $wpdb->get_results("SELECT * FROM tbl_proofreader_general_details WHERE fk_proofreader_id = $proofreader_id LIMIT 1");
        if ($general_details) {
            $word_count = $general_details[0]->total_words_edited + $total;
            $result = $wpdb->update('tbl_proofreader_general_details', array(
                'total_docs_worked' => $doc_count,
                'total_words_edited' => $word_count,
                'total_earning' => $total_amt,
                'total_paid_amount' => $paid_amt,
                'remaining_amount' => $remaining_amt
                    ), array('fk_proofreader_id' => $proofreader_id)
            );
        } else {
            $word_count = $total;
            $result = $wpdb->insert('tbl_proofreader_general_details', array(
                'fk_proofreader_id' => $proofreader_id,
                'total_docs_worked' => $doc_count,
                'total_words_edited' => $word_count,
                'total_earning' => $total_amt,
                'total_paid_amount' => $paid_amt,
                'remaining_amount' => $remaining_amt
                    )
            );
        }

        $main_doc = $wpdb->get_results("SELECT * FROM `wp_customer_document_main`  Where pk_document_id =  $fk_main_doc_id AND Status =1 LIMIT 1 ");
        $main_docName = $main_doc[0]->document_title;
        $cust_id = $main_doc[0]->fk_customer_id;
        $cust_info = get_userdata($cust_id);
        $result = $wpdb->insert('tbl_proofreader_history', array(
            "fk_proofreader_id" => $proofreader_id,
            "date" => date('Y-m-d H:i:s'),
            'doc_name' => $main_docName,
            "customer" => $cust_info->first_name . ' ' . $cust_info->last_name,
            'total_words' => $main_doc[0]->word_count,
            "edited_words" => $total,
            "earned" => $amount,
            "created_date" => date('Y-m-d H:i:s')
        ));
    }
// var_dump($data);

    foreach ($data as $value) {

        $result = $wpdb->insert('tbl_document_word_tracking', array(
            "wrong_word" => $value[2],
            "wrong_start_offset" => 0,
            "wrong_end_offset" => 0,
            "wrong_length" => $value[0],
            "fk_main_doc_id" => $fk_main_doc_id,
            "fk_subdoc_id" => $doc_id,
            "fk_proofreader_id" => $user_id,
            "corrected_word" => $value[2],
            "corrected_word_start_offset" => 0,
            "corrected_word_end_offset" => 0,
            "corrected_word_length" => $value[0],
            "Action" => $value[1],
            "createddate" => date('Y-m-d H:i:s'),
            "modifieddate" => date('Y-m-d H:i:s'),
            "check_status" => "Single Check"
        ));
    }


    echo 'Document Submitted Successfully';

    die(0);
}

//end
//save user upload document value

add_action('wp_ajax_nopriv_save_user_upload_doc', 'save_user_upload_doc_callback');
add_action('wp_ajax_save_user_upload_doc', 'save_user_upload_doc_callback');

function save_user_upload_doc_callback() {

    global $wpdb;
    $desc = stripslashes($_POST['word_desc']);
    $doc_title = stripslashes($_POST['doc_title']);
    $user_id = get_current_user_id();
    $datetime = date('Y-m-d H:i:s');

    $user_id = get_current_user_id();

     $orders = $wpdb->get_results("SELECT max(order_no) as max_order FROM `wp_customer_document_main` where Status=1");
    $max_order = $orders[0]->max_order;

    $doc_number = 1;
    $result = $wpdb->get_results("SELECT * FROM `wp_customer_document_main` WHERE fk_customer_id = $user_id AND Status =1 ORDER BY pk_document_id DESC LIMIT 1 ");
    if (count($result) > 0) {
        $doc_number = $result[0]->doc_number + 1;
    }

    if ($desc != '') {
        $desc = str_replace("\n"," \n", $desc);
        $totalNoOfWords = get_word_count($desc);

        $count = $_POST['word_count'];

        $status = true;

        $prefix = $wpdb->prefix;
        $table_name = $prefix . 'customer_document_main';

        $timestamp = strtotime($datetime);

        $wpdb->insert(
                $table_name, array(
            'fk_customer_id' => $user_id,
            'word_count' => $totalNoOfWords,
            'doc_number' => $doc_number,
            'document_title' => $doc_title,
            'upload_date' => $datetime,
            'document_desc' => $desc,
            'Status' => $status,
            'created_date' => $datetime,
            'order_no' => $max_order + 1
                )
        );
        $pk_doc_main_id = $wpdb->insert_id;
        $num_of_chunk = 0;
        $remaing_word = 0;
        $is_flag = 0;

        if ($totalNoOfWords <= 600) {
            $strt = 0;
            $end = 0;

            $table_name = $prefix . 'customer_document_details';
            $wpdb->insert(
                    $table_name, array(
                'fk_doc_main_id' => $pk_doc_main_id, //$user_id,
                'word_count' => $totalNoOfWords,
                'document_desc' => $desc,
                'status' => 'Pending',
                'word_start_no' => $strt,
                'word_end_no' => $totalNoOfWords,
                'fk_cust_id' => $user_id,
                'is_active' => true,
                'created_date' => $datetime
                    )
            );
        } else if ($totalNoOfWords > 600 && $totalNoOfWords <= 1000) {

            $minus_words_limit = 50;
            $start = 0;
            $max_words_limit = floor($totalNoOfWords / 2);
            $pos_count = $start;
            $minusPosition = $max_words_limit - $minus_words_limit;

            $chunk_string = implode(' ', array_slice(explode(' ', $desc), $minusPosition, $minus_words_limit));

            $pos_FullStop = strrpos($chunk_string, '.');
            $pos_Ques_Mark = strrpos($chunk_string, '?');
            $pos_exlimation = strrpos($chunk_string, '!');

            if ($pos_FullStop != 0) {
                $withOutDot = substr($chunk_string, 0, $pos_FullStop) . '.';
                $pos_count = get_word_count($withOutDot);
            } else if ($pos_Ques_Mark != 0) {
                $withOutDot = substr($chunk_string, 0, $pos_Ques_Mark) . '?';
                $pos_count = get_word_count($withOutDot);
            } else if ($pos_exlimation != 0) {
                $withOutDot = substr($chunk_string, 0, $pos_exlimation) . '!';
                $pos_count = get_word_count($withOutDot);
            }

            $endPosition = $minusPosition + $pos_count;
            $Max_words = $endPosition - $start;

            $final_chunk_result = implode(' ', array_slice(explode(' ', $desc), $start, $Max_words));
            $table_name = $prefix . 'customer_document_details';
            $wpdb->insert(
                    $table_name, array(
                'fk_doc_main_id' => $pk_doc_main_id,
                'word_count' => $totalNoOfWords,
                'document_desc' => $final_chunk_result,
                'status' => 'Pending',
                'word_start_no' => $start,
                'word_end_no' => $endPosition,
                'fk_cust_id' => $user_id,
                'is_active' => true,
                'created_date' => $datetime,
                    )
            );

            $start = $endPosition;
            $endPosition = $totalNoOfWords;

            if ($max_words_limit > $totalNoOfWords)
                $max_words_limit = $totalNoOfWords;

            $Max_words = $totalNoOfWords - $start;

            $final_chunk_result = implode(' ', array_slice(explode(' ', $desc), $start, $Max_words));

            $table_name = $prefix . 'customer_document_details';
            $wpdb->insert(
                    $table_name, array(
                'fk_doc_main_id' => $pk_doc_main_id,
                'word_count' => $totalNoOfWords,
                'document_desc' => $final_chunk_result,
                'status' => 'Pending',
                'word_start_no' => $start,
                'word_end_no' => $endPosition,
                'fk_cust_id' => $user_id,
                'is_active' => true,
                'created_date' => $datetime,
                    )
            );
        } else if ($totalNoOfWords > 1000) {
            $max_words_limit = 500;
            $const_max_words_limit = $max_words_limit;
            $minus_words_limit = 50;

            $remaing_word = $totalNoOfWords % $max_words_limit;
            $num_of_chunk = $totalNoOfWords / $max_words_limit;
            $num_of_chunk = floor($num_of_chunk);

            $start = 0;
            for ($i = 1; $i <= $num_of_chunk; $i++) {

                $pos_count = 0;
                $minusPosition = $max_words_limit - $minus_words_limit;
                $chunk_string = implode(' ', array_slice(explode(' ', $desc), $minusPosition, $minus_words_limit));

                $pos_FullStop = strrpos($chunk_string, '.');
                $pos_Ques_Mark = strrpos($chunk_string, '?');
                $pos_exlimation = strrpos($chunk_string, '!');

                if ($pos_FullStop != 0) {
                    $withOutDot = substr($chunk_string, 0, $pos_FullStop) . '.';
                    $pos_count = get_word_count($withOutDot);
                } else if ($pos_Ques_Mark != 0) {
                    $withOutDot = substr($chunk_string, 0, $pos_Ques_Mark) . '?';
                    $pos_count = get_word_count($withOutDot);
                } else if ($pos_exlimation != 0) {
                    $withOutDot = substr($chunk_string, 0, $pos_exlimation) . '!';
                    $pos_count = get_word_count($withOutDot);
                }

                $endPosition = $minusPosition + $pos_count;
                $max_words_limit = $endPosition + $const_max_words_limit;

                if ($i == $num_of_chunk) {

                    $remaining_total_word = $totalNoOfWords - $start;
                    if ($remaining_total_word <= 600) {

                        $Max_words = $endPosition - $start + ($totalNoOfWords - $endPosition);
                        $final_chunk_result = implode(' ', array_slice(explode(' ', $desc), $start, $Max_words));
                        $endPosition = $totalNoOfWords;
                        $table_name = $prefix . 'customer_document_details';
                        $wpdb->insert(
                                $table_name, array(
                            'fk_doc_main_id' => $pk_doc_main_id,
                            'word_count' => $totalNoOfWords,
                            'document_desc' => $final_chunk_result,
                            'status' => 'Pending',
                            'word_start_no' => $start,
                            'word_end_no' => $endPosition,
                            'fk_cust_id' => $user_id,
                            'is_active' => true,
                            'created_date' => $datetime,
                                )
                        );
                    } else {

                        $max_words_limit = floor($remaining_total_word / 2);
                        $num_of_chunk = 2;
                        $pos_count = $start;
                        $minusPosition = $start + $max_words_limit - $minus_words_limit;

                        $chunk_string = implode(' ', array_slice(explode(' ', $desc), $minusPosition, $minus_words_limit));

                        $pos_FullStop = strrpos($chunk_string, '.');
                        $pos_Ques_Mark = strrpos($chunk_string, '?');
                        $pos_exlimation = strrpos($chunk_string, '!');

                        if ($pos_FullStop != 0) {
                            $withOutDot = substr($chunk_string, 0, $pos_FullStop) . '.';
                            $pos_count = get_word_count($withOutDot);
                        } else if ($pos_Ques_Mark != 0) {
                            $withOutDot = substr($chunk_string, 0, $pos_Ques_Mark) . '?';
                            $pos_count = get_word_count($withOutDot);
                        } else if ($pos_exlimation != 0) {
                            $withOutDot = substr($chunk_string, 0, $pos_exlimation) . '!';
                            $pos_count = get_word_count($withOutDot);
                        }

                        $endPosition = $minusPosition + $pos_count;
                        $Max_words = $endPosition - $start;

                        $final_chunk_result = implode(' ', array_slice(explode(' ', $desc), $start, $Max_words));
                        $table_name = $prefix . 'customer_document_details';
                        $wpdb->insert(
                                $table_name, array(
                            'fk_doc_main_id' => $pk_doc_main_id,
                            'word_count' => $totalNoOfWords,
                            'document_desc' => $final_chunk_result,
                            'status' => 'Pending',
                            'word_start_no' => $start,
                            'word_end_no' => $endPosition,
                            'fk_cust_id' => $user_id,
                            'is_active' => true,
                            'created_date' => $datetime,
                                )
                        );

                        $start = $endPosition;
                        $endPosition = $totalNoOfWords;

                        if ($max_words_limit > $totalNoOfWords)
                            $max_words_limit = $totalNoOfWords;

                        $Max_words = $totalNoOfWords - $start;

                        $final_chunk_result = implode(' ', array_slice(explode(' ', $desc), $start, $Max_words));

                        $table_name = $prefix . 'customer_document_details';
                        $wpdb->insert(
                                $table_name, array(
                            'fk_doc_main_id' => $pk_doc_main_id,
                            'word_count' => $totalNoOfWords,
                            'document_desc' => $final_chunk_result,
                            'status' => 'Pending',
                            'word_start_no' => $start,
                            'word_end_no' => $endPosition,
                            'fk_cust_id' => $user_id,
                            'is_active' => true,
                            'created_date' => $datetime,
                                )
                        );
                    }
                } else {

                    $Max_words = $endPosition - $start;
                    $final_chunk_result = implode(' ', array_slice(explode(' ', $desc), $start, $Max_words));

                    $table_name = $prefix . 'customer_document_details';
                    $wpdb->insert(
                            $table_name, array(
                        'fk_doc_main_id' => $pk_doc_main_id,
                        'word_count' => $totalNoOfWords,
                        'document_desc' => $final_chunk_result,
                        'status' => 'Pending',
                        'word_start_no' => $start,
                        'word_end_no' => $endPosition,
                        'fk_cust_id' => $user_id,
                        'is_active' => true,
                        'created_date' => $datetime,
                            )
                    );
                    $start = $endPosition;

                    if ($max_words_limit > $totalNoOfWords)
                        $max_words_limit = $totalNoOfWords;
                }
            }
        }

        $result_general = $wpdb->get_results("SELECT * FROM `tbl_customer_general_info` WHERE fk_customer_id = $user_id LIMIT 1 ");
        $result_doc_main = $wpdb->get_results("SELECT * FROM `wp_customer_document_main` WHERE fk_customer_id = $user_id AND Status=1 ");
        $totaldocs = count($result_doc_main);

        if (count($result_general) > 0) {
            $remaining_credit = $result_general[0]->remaining_credit_words;
            $result = $wpdb->update(
                    'tbl_customer_general_info', array
                (
                'total_submited_docs' => $totaldocs,
                'remaining_credit_words' => $remaining_credit - $totalNoOfWords,
                "modified_date" => date('Y-m-d H:i:s')
                    ), array('fk_customer_id' => $user_id));
        } else {
            $result = $wpdb->insert(
                    'tbl_customer_general_info', array(
                'total_submited_docs' => $totaldocs,
                'created_date' => date('Y-m-d H:i:s')
                    )
            );
        }


        $blogusers = get_users(array('role' => 'proofreader', 'meta_query' => array(
                'relation' => 'AND',
                array(
                    'key' => 'test_completed',
                    'value' => TRUE,
                    'compare' => '='
                ),
                array(
                    'key' => 'test_status',
                    'value' => 'accepted',
                    'compare' => '='
                ),
            )
        ));
        foreach ($blogusers as $users) {
            $proof_id = $users->ID;
            $assigned_document = $wpdb->get_results("SELECT * FROM wp_assigned_document_details WHERE fk_proofreader_id= $proof_id AND status = 'Pending' OR status='Single Check'" . "");
            $assigned_count = count($assigned_document);
//            if ($assigned_count < 1 || $assigned_count == '') {
            $result_user = $wpdb->get_results("SELECT * FROM wp_proofreader_notification_setting WHERE user_id= $proof_id");
            $desk_noti = $result_user[0]->desktop_new_doc;

            $subject = 'New Writesaver Documents Available';
            $descs = "Just wanted to let you know that there are new documents available for proofreading! Login to your proofreader dashboard and start editing now to start earning.";
            $descs .= "<br/><br/><strong><em><a href='" . get_permalink(810) . "' >Access your proofreader dashboard</a></em></strong>";
            send_proof_notification($user_id, $proof_id, $descs, $desk_noti, 1, $subject);
//            }
        }

        $pkdocid = $pk_doc_main_id;
        $subdocsdeatils = $wpdb->get_results("SELECT * FROM wp_customer_document_details WHERE fk_doc_main_id= $pkdocid AND is_active = 1");
        $cntLine = 1;
        foreach ($subdocsdeatils as $subdc) {
            $user_id = $user->ID;
            $str = $subdc->document_desc;
            $arr = explode("\r\n", $str);
            $Fk_sub_doc_id = $subdc->pk_doc_details_id;
            foreach ($arr as $key => $value) {
                global $wpdb;
                $table_name = 'tbl_doc_line_details';
                $wpdb->insert(
                        $table_name, array(
                    'Fk_main_doc_id' => $pkdocid,
                    'Fk_sub_doc_id' => $Fk_sub_doc_id,
                    'Fk_line_id' => $cntLine,
                    'Line_details' => $value,
                    'Fk_user_id' => $user_id
                        )
                );
                ++$cntLine;
            }
        }
        echo 'Your document has been submitted, and will be ready within 24 hours';
    } else {
        echo 'error';
    }
    die(0);
}

/* Disable WordPress Admin Bar for all users but admins. */
add_action('after_setup_theme', 'remove_admin_bar');

function remove_admin_bar() {
    if (!current_user_can('administrator') && !is_admin()) {
        show_admin_bar(false);
    }
}

// For active class in menu
function special_nav_class($classes, $item) {
    if (in_array('current-menu-item', $classes)) {
        $classes[] = 'active ';
    }
    return $classes;
}

add_filter('nav_menu_css_class', 'special_nav_class', 10, 2);



// CUSTOMER NOTIFICATION SETTINGS
add_action('wp_ajax_nopriv_fnupdatecustomernotification', 'fnupdatecustomernotification_callback');
add_action('wp_ajax_fnupdatecustomernotification', 'fnupdatecustomernotification_callback');

function fnupdatecustomernotification_callback() {

    if (is_user_logged_in()) {
        $user_id = get_current_user_id();
        $field_value = $_POST['fieldvalue'];
        $field_name = $_POST['fieldname'];

        if ($field_value == 'true') {
            $status = TRUE;
        } else {
            $status = FALSE;
        }

        global $wpdb;
        $prefix = $wpdb->prefix;
        $table_name = $prefix . 'notification_settings';
        $result = $wpdb->get_var($wpdb->prepare("SELECT count(*) FROM $table_name WHERE user_id = %d ", $user_id));

        if ($result > 0) {
// UPDATE DATA
            if ($field_name == 'dash_doc_started') {
                $result = $wpdb->update(
                        $table_name, array
                    (
                    'dash_doc_started' => $status,
                    "modifieddate" => date('Y-m-d H:i:s')
                        ), array('user_id' => $user_id));
            } else if ($field_name == 'dash_doc_completed') {
                $result = $wpdb->update(
                        $table_name, array
                    (
                    'dash_doc_completed' => $status,
                    "modifieddate" => date('Y-m-d H:i:s')
                        ), array('user_id' => $user_id));
            } else if ($field_name == 'receive_doc_started') {
                $result = $wpdb->update(
                        $table_name, array
                    (
                    'receive_doc_started' => $status,
                    "modifieddate" => date('Y-m-d H:i:s')
                        ), array('user_id' => $user_id));
            } else if ($field_name == 'receive_doc_completed') {
                $result = $wpdb->update(
                        $table_name, array
                    (
                    'receive_doc_completed' => $status,
                    "modifieddate" => date('Y-m-d H:i:s')
                        ), array('user_id' => $user_id));
            } else if ($field_name == 'receive_stories') {
                $result = $wpdb->update(
                        $table_name, array
                    (
                    'receive_stories' => $status,
                    "modifieddate" => date('Y-m-d H:i:s')
                        ), array('user_id' => $user_id));
            }
        } else {
// INSERT DATA
            if ($field_name == 'dash_doc_started') {

                $result = $wpdb->insert($table_name, array(
                    "user_id" => $user_id,
                    "dash_doc_started" => $status,
                    "createddate" => date('Y-m-d H:i:s')
                ));
            } else if ($field_name == 'dash_doc_completed') {

                $result = $wpdb->insert($table_name, array(
                    "user_id" => $user_id,
                    "dash_doc_completed" => $status,
                    "createddate" => date('Y-m-d H:i:s')
                ));
            } else if ($field_name == 'receive_doc_started') {

                $result = $wpdb->insert($table_name, array(
                    "user_id" => $user_id,
                    "receive_doc_started" => $status,
                    "createddate" => date('Y-m-d H:i:s')
                ));
            } else if ($field_name == 'receive_doc_completed') {

                $result = $wpdb->insert($table_name, array(
                    "user_id" => $user_id,
                    "receive_doc_completed" => $status,
                    "createddate" => date('Y-m-d H:i:s')
                ));
            } else if ($field_name == 'receive_stories') {

                $result = $wpdb->insert($table_name, array(
                    "user_id" => $user_id,
                    "receive_stories" => $status,
                    "createddate" => date('Y-m-d H:i:s')
                ));
            }
        }
    } else {
        wp_redirect(home_url());
    }
    die(0);
}

add_action('wp_ajax_nopriv_save_profile', 'save_profile_callback');
add_action('wp_ajax_save_profile', 'save_profile_callback');

function save_profile_callback() {

    $user_ID = get_current_user_id();
    $formdata = $_POST["alldata"];
    $hdnremoveimage = $_POST["hdnremoveimage"];
    parse_str($formdata);

    $user_info = get_userdata($user_id);
    $upload_overrides = array('test_form' => false);
    $attach_image_file = '';

    if ($hdnremoveimage == 1) {
        update_user_meta($user_ID, 'profile_pic_url', '');
        update_user_meta($user_ID, 'thechamp_large_avatar', '');
    } else {

//if ($_FILES) {
        if ($_FILES['profile_pic']['name']) {
            foreach ($_FILES as $file) {
                $attach_image_file = wp_handle_upload($file, $upload_overrides);
                $filename = $attach_image_file['file'];
                $fileurl = $attach_image_file['url'];
                update_user_meta($user_ID, 'profile_pic_url', $fileurl);
                update_user_meta($user_ID, 'thechamp_large_avatar', $fileurl);
            }
        }
    }

    $update = update_user_meta($user_ID, 'first_name', $pro_fname);
    $update = update_user_meta($user_ID, 'last_name', $pro_lname);

    echo '1';
    die(0);
}

/* End Ajax call for update  profile information */

/* Ajax call for Change Password */
add_action('wp_ajax_nopriv_save_pass', 'save_pass_callback');
add_action('wp_ajax_save_pass', 'save_pass_callback');

function save_pass_callback() {
    require_once( ABSPATH . WPINC . '/class-phpass.php');
    $user_ID = get_current_user_id();
    $pass = $_POST['pass'];
    $new_pass = $_POST['new_pass'];
    $conf_pass = $_POST['conf_pass'];
    $user_info = get_userdata($user_ID);
    $wp_hasher = new PasswordHash(8, TRUE);
    if ($wp_hasher->CheckPassword($pass, $user_info->user_pass)) {
        $user_data = array(
            'ID' => $user_ID,
            'user_pass' => $new_pass
        );
        wp_update_user($user_data);
        echo 1;
    } else {
        echo 0;
    }
    die;
}

/* End Ajax call for Change Password  */


/* profile_notification_setting START */

add_action('wp_ajax_nopriv_fnupdatenotificationsettings', 'fnupdatenotificationsettings_callback');
add_action('wp_ajax_fnupdatenotificationsettings', 'fnupdatenotificationsettings_callback');

function fnupdatenotificationsettings_callback() {
    if (is_user_logged_in()) {
        $user_id = get_current_user_id();
        $field_value = $_POST['fieldvalue'];
        $field_name = $_POST['fieldname'];

        if ($field_value == 'true') {
            $status = TRUE;
        } else {
            $status = FALSE;
        }

        global $wpdb;
        $prefix = $wpdb->prefix;
        $table_name = $prefix . 'proofreader_notification_setting';


        $result = $wpdb->get_var($wpdb->prepare("SELECT count(*) FROM $table_name WHERE user_id = %d ", $user_id));

        if ($result > 0) {
// UPDATE DATA 
            $result = $wpdb->update($table_name, array(
                $field_name => $status,
                "modified_date" => date('Y-m-d H:i:s')
                    ), array('user_id' => $user_id));

//echo 'update saved';
        } else {
// INSERT DATA
            $result = $wpdb->insert($table_name, array(
                "user_id" => $user_id,
                $field_name => $status,
                "created_date" => date('Y-m-d H:i:s')
            ));

//echo 'inserted';    
        }
    } else {
        wp_redirect(home_url());
    }
    die(0);
}

/* profile_notification_setting END */


add_action('wp_ajax_nopriv_save_free_api_result', 'save_free_api_result_callback');
add_action('wp_ajax_save_free_api_result', 'save_free_api_result_callback');

function save_free_api_result_callback() {

    $doc_id = $_POST['doc_id'];
    $result = "";
    $array = urldecode($_POST['data2']);
//$someArray =var_dump(json_decode($array, true));
// $array = var_dump(json_decode(json_encode($array)));
//  print_r($array->errors);
    foreach ($array[0]['errors'] as $obj) {
//$result=$result.''.$obj['bad'];
        print_r($obj);
        die(0);
    }
//    
//echo $result;
    die(0);
}

// test post type START//
$testexam_labels = array(
    'name' => _x('Test', 'post type general name'),
    'singular_name' => _x('Test', 'post type singular name'),
    'add_new' => _x('Add New', 'ocr_gallery'),
    'add_new_item' => __("Add New Test"),
    'edit_item' => __("Edit Test"),
    'new_item' => __("New Test"),
    'view_item' => __("View Test"),
    'search_items' => __("Search Test"),
    'not_found' => __('No Test Exam Saving found'),
    'not_found_in_trash' => __('No Test Exam found in Trash'),
    'parent_item_colon' => ''
);
$testexam_args = array(
    'labels' => $testexam_labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => true,
    'hierarchical' => false,
    'menu_position' => null,
    'capability_type' => 'post',
    'supports' => array('title', 'editor', 'excerpt', 'custom-fields', 'feature-image', 'thumbnail'),
    'taxonomies' => array('category'),
    'can_export' => true,
    'has_archive' => true,
    'exclude_from_search' => false,
    'publicly_queryable' => true,
);
register_post_type('testexam', $testexam_args);

// test post type END//


add_filter('wp_nav_menu_items', 'wti_loginout_menu_link', 10, 2);

function wti_loginout_menu_link($items, $args) {
    if ($args->theme_location == 'customer-dropdown-sub-menu' || $args->theme_location == 'proofreader-dropdown-sub-menu') {
        $items .= '<li><a href="' . wp_logout_url(home_url()) . '">' . __("Log Out") . '</a></li>';
    }
    return $items;
}

add_shortcode('proofreader_dashboard_information', 'fn_proofreader_dashboard_information');

function fn_proofreader_dashboard_information() {


    global $wpdb;
    $user_id = get_current_user_id();
    $result = $wpdb->get_results("SELECT * FROM tbl_proofreader_general_details WHERE fk_proofreader_id= $user_id LIMIT 1 ");

    $result_available_assigned_docs = $wpdb->get_results("SELECT * FROM wp_assigned_document_details WHERE fk_proofreader_id= $user_id and status='In Process' ");
    $result_available_doc_Sections = $wpdb->get_results("SELECT * FROM wp_customer_document_details WHERE status='Pending' AND is_active=1");
    $result_available_double_check_doc = $wpdb->get_results("SELECT * FROM tbl_proofreaded_doc_details where  (Fk_DoubleProofReader_Id =$user_id OR Fk_DoubleProofReader_Id IS NULL)  AND  fk_proofreader_id !=$user_id AND DoubleCheckStatus != 1");

    $total_docs_worked = 0;
    $total_words_edited = 0;
    $total_earning = 0;

    if (count($result) > 0) {
        $total_docs_worked = ($result[0]->total_docs_worked == "" || $result[0]->total_docs_worked == null ? 0 : $result[0]->total_docs_worked);
        $total_words_edited = ($result[0]->total_words_edited == "" || $result[0]->total_words_edited == null ? 0 : $result[0]->total_words_edited );
        $total_earning = ($result[0]->total_earning == "" || $result[0]->total_earning == null ? 0 : $result[0]->total_earning);
    }


    $htmlData = '';

    $htmlData .= '<section class="proof_dashboard">';
    $htmlData .= '<div class="container">';
    $htmlData .= '<div class="all_page_proofer">';

    $info = get_user_meta($user_id, 'info_completed', true);
    $test = get_user_meta($user_id, 'test_completed', true);
    $test_status = get_user_meta($user_id, 'test_status', TRUE);

    if ($info == 1) {
        if ($test == 1) {
            if ($test_status == 'accepted') {
                if (count($result_available_assigned_docs) > 0 || count($result_available_doc_Sections) > 0 || count($result_available_double_check_doc) > 0) {

                    $proof_url = get_the_permalink(852);
                    $label = "Ready to start earning?";
                    $btn_label = 'Start proofreading';
                } else {
                    $label = "No documents are currently available for proofreading.";
                }
            } else {
                if ($test_status == 'delayed')
                    $label = "Your application has been put on hold by our team. Check back soon!";
                else
                    $label = "Your proofreading application has not yet been reviewed by our team. You'll receive an update soon!";
            }
        } else {
            $proof_url = get_the_permalink(774);
            $label = "You have not completed all of the tests yet.";
            $btn_label = 'Start Test';
        }
    } else {
        $proof_url = get_the_permalink(770);
        $label = "You have not inputted all of your information.";
        $btn_label = 'Basic Information';
    }

    $htmlData .= '<div class="completed_all_test_content">';

    $htmlData .= '<h2>' . $label . '</h2>';
    $htmlData .= '</div>';
    $htmlData .= '<div class="btn_blue completed_test">';
    if ($proof_url != '' && $btn_label != ''):
        $htmlData .= '<a href="' . $proof_url . '" class="btn_sky">' . $btn_label . '</a>';
    endif;

    $htmlData .= '</div>';
    $htmlData .= '</div>';
    $htmlData .= '</div>';
    $htmlData .= '</section>';

    $htmlData .= '<section class="proof privacy">';
    $htmlData .= '<div class="container">';
    $htmlData .= '<div class="row service">';
    $htmlData .= '<div class="col-sm-4">';
    $htmlData .= '<div class="total_ammount">';
    $htmlData .= '<div class="left">';
    $htmlData .= '<h4>' . $total_docs_worked . '<span>Docs.</span></h4>';
    $htmlData .= '<div class="divider"></div>';
    $htmlData .= '<p>Total <br>Docs. worked on</p>';
    $htmlData .= '</div>';
    $htmlData .= '<div class="right"></div>';
    $htmlData .= '</div>';
    $htmlData .= '</div>';
    $htmlData .= '<div class="col-sm-4">';
    $htmlData .= '<div class="total_ammount paid">';
    $htmlData .= '<div class="left">';
    $htmlData .= '<h4>' . $total_words_edited . '<span>Words</span></h4>';
    $htmlData .= '<div class="divider"></div>';
    $htmlData .= '<p>Total<br> words edited</p>';
    $htmlData .= '</div>';
    $htmlData .= '<div class="right"></div>';
    $htmlData .= '</div>';
    $htmlData .= '</div>';
    $htmlData .= '<div class="col-sm-4">';
    $htmlData .= '<div class="total_ammount remaining">';
    $htmlData .= '<div class="left">';
    $htmlData .= '<h4>$' . number_format((float) $total_earning, 2, '.', '') . '<span>Money</span></h4>';
    $htmlData .= '<div class="divider"></div>';
    $htmlData .= '<p>Total<br> earnings</p>';
    $htmlData .= '</div>';
    $htmlData .= '<div class="right"></div>';
    $htmlData .= '</div>';
    $htmlData .= '</div>';
    $htmlData .= '</div>';
    $htmlData .= '</div>';
    $htmlData .= '</section>';

    echo $htmlData;
}

/* profile_notification_setting END */


add_action('wp_ajax_nopriv_save_Word_Changes', 'save_Word_Changes_callback');
add_action('wp_ajax_save_Word_Changes', 'save_Word_Changes_callback');

function save_Word_Changes_callback() {
    global $wpdb;
    $user_id = $_POST['userid'];
    $doc_id = $_POST['doc_id'];
    $result = "";
    $array = ($_POST['data2']);
//  print_r($array);

    $data = array();
    foreach ($array as $tarea) {
        $data[] = $tarea;
    }
// var_dump($data);
    foreach ($data as $value) {
//$result .="offset ".$value[0]."length ".$value[1].$value[2].$value[3].$value[4];
        $endoffset = $value[0] + $value[1];
        $result = $wpdb->insert('tbl_document_word_tracking', array(
            "wrong_word" => $value[2],
            "wrong_start_offset" => $value[0],
            "wrong_end_offset" => $endoffset,
            "wrong_length" => $value[1],
            "fk_subdoc_id" => $doc_id,
            "fk_proofreader_id" => $user_id,
            "corrected_word" => $value[3],
            "corrected_word_start_offset" => $value[0],
            "corrected_word_end_offset" => $endoffset,
            "corrected_word_length" => $value[1],
            "Action" => $value[4],
            "createddate" => date('Y-m-d H:i:s'),
            "modifieddate" => date('Y-m-d H:i:s')
        ));
    }
//    
    echo "success";
    die(0);
}

/* Ajax call for get doc information */
add_action('wp_ajax_nopriv_get_selected_doc_data', 'get_selected_doc_data_callback');
add_action('wp_ajax_get_selected_doc_data', 'get_selected_doc_data_callback');

function get_selected_doc_data_callback() {
    global $wpdb;
    $user_id = get_current_user_id();

    $main_doc_id = $_POST['main_doc_id'];
    $main_doc_info = $wpdb->get_results(" SELECT * FROM `wp_customer_document_main` WHERE pk_document_id = $main_doc_id  AND Status=1");

    $doc_info = $wpdb->get_results(" SELECT * FROM `wp_customer_document_details` WHERE fk_cust_id = $user_id AND fk_doc_main_id = $main_doc_id AND is_active");

    $htmlData = "";
    $doc_count = 1;
    foreach ($doc_info as $value) {

        if ($value->status == 'Pending' || $value->status == 'In Process') {
            $htmlData .= "<p  contenteditable='false' data-section=" . $doc_count++ . " data-id='$value->pk_doc_details_id'>" . str_replace("\n", "<br>", $value->document_desc) . "</p>";
        } else {
            $doc_info = $wpdb->get_results(" SELECT * FROM `tbl_proofreaded_doc_details` WHERE fk_doc_details_id = $value->pk_doc_details_id LIMIT 1 ");
            $htmlData .= "<p  contenteditable='false' data-section=" . $doc_count++ . " data-id='$value->pk_doc_details_id' >" . str_replace("\n", "<br>", $doc_info[0]->doc_desc) . "</p>";
        }
    }


    echo json_encode(array($htmlData, $main_doc_info[0]->document_title));
    die();
}

/* Ajax call for get proofreader related customer */
add_action('wp_ajax_nopriv_getProofreaderByCustomerId', 'getProofreaderByCustomerId_callback');
add_action('wp_ajax_getProofreaderByCustomerId', 'getProofreaderByCustomerId_callback');

function getProofreaderByCustomerId_callback() {
    global $wpdb;
    $user_id = get_current_user_id();

    $main_doc_id = $_POST['main_doc_id'];

// $proofreader = $wpdb->get_results(" SELECT fk_proofreader_id,fk_cust_id  FROM wp_assigned_document_details WHERE fk_cust_id=$user_id and fk_doc_main_id=$main_doc_id");
    $proofreader = $wpdb->get_results("SELECT wp_assigned_document_details.fk_proofreader_id,wp_assigned_document_details.fk_cust_id,wp_assigned_document_details.fk_doc_details_id,"
            . "wp_customer_document_details.document_desc FROM wp_assigned_document_details join wp_customer_document_details on wp_assigned_document_details.fk_doc_details_id=wp_customer_document_details.pk_doc_details_id "
            . "WHERE wp_assigned_document_details.fk_cust_id=$user_id and wp_assigned_document_details.fk_doc_main_id=$main_doc_id AND wp_customer_document_details.status='In Process'");
    if (count($proofreader) == 0) {
        $proofreader = $wpdb->get_results("SELECT fk_proofreader_id,fk_cust_id,fk_doc_details_id,doc_desc as 'document_desc' FROM tbl_proofreaded_doc_details where fk_cust_id=$user_id and fk_doc_main_id=$main_doc_id and status!='Completed'");
    }
    echo json_encode(array($proofreader));
    die(0);
}

/* Ajax call for update Country information */
add_action('wp_ajax_nopriv_save_country', 'save_country_callback');
add_action('wp_ajax_save_country', 'save_country_callback');

function save_country_callback() {

    $user_ID = get_current_user_id();
    $country_citizenship = $_POST['country_citizenship'];
    $country_cresidence = $_POST['country_cresidence'];

    $update = update_user_meta($user_ID, 'country_citizenship', $country_citizenship);
    $update = update_user_meta($user_ID, 'country_cresidence', $country_cresidence);
    if ($_POST['info'])
        $update = update_user_meta($user_ID, 'info_completed', 1);

    echo 1;
    die(0);
}

/* End Ajax call for update University information */

/* Ajax call for update University information */
add_action('wp_ajax_nopriv_save_university', 'save_university_callback');
add_action('wp_ajax_save_university', 'save_university_callback');

function save_university_callback() {

    $user_ID = get_current_user_id();
    $education = $_POST['education'];
    $education = stripslashes_deep($education);
    $edu_count = 0;
    foreach ($education as $edu) {
        $edu = json_decode($edu, TRUE);
        $edu_count++;
        $update = update_user_meta($user_ID, 'degree' . $edu_count, $edu['edu_degree']);
        $update = update_user_meta($user_ID, 'university' . $edu_count, $edu['edu_uni']);
    }

    $update = update_user_meta($user_ID, 'total_edu', $edu_count);
    echo 1;
    die(0);
}

/* End Ajax call for update University information */

/* Ajax call for update basic information */
add_action('wp_ajax_nopriv_save_basic', 'save_basic_callback');
add_action('wp_ajax_save_basic', 'save_basic_callback');

function save_basic_callback() {

    $user_ID = get_current_user_id();

    $fname = $_POST['fname'];
    $lname = $_POST['lname'];
    $add1 = $_POST['add1'];
    $add2 = $_POST['add2'];
    $zip_code = $_POST['zip_code'];
    $P_state = $_POST['P_state'];
    $remove = $_POST['remove_image'];

    if ($remove == 1) {
        update_user_meta($user_ID, 'profile_pic_url', '');
        update_user_meta($user_ID, 'thechamp_large_avatar', '');
        $data = array('image' => '', 'message' => '<span class="text-success pic_msg1">Updated successfully...</span>');
    } else {
        if ($_FILES['profile_pic']['name']) {

            if (0 < $_FILES['file']['error']) {
                $filename = get_user_meta($user_ID, 'profile_pic_url', TRUE);
                if ($filename == '') {
                    update_user_meta($user_ID, 'profile_pic_url', '');
                    update_user_meta($user_ID, 'thechamp_large_avatar', '');
                    $data = array('image' => '', 'message' => '<span class="text-success pic_msg1">Profile Picture not uploaded.</span>');
                } else {
                    $data = array('image' => $filename, 'message' => '<span class="text-success pic_msg1">Profile Picture not uploaded.</span>');
                }
            } else {
                $uploadFileName = $_FILES['profile_pic']['name'];
                if (isset($uploadFileName) && !empty($uploadFileName)) {

                    $upload_overrides = array('test_form' => FALSE);
                    $attach_image_file = '';
                    $attach_image_file = wp_handle_upload($_FILES['profile_pic'], $upload_overrides);
                    $fileurl = $attach_image_file['url'];

//Delete old profile form
                    $filename = get_user_meta($user_ID, 'profile_pic_url', TRUE);
                    $urlparts = parse_url($filename);
                    $extracted = $urlparts['path'];
                    ltrim($extracted, '/');
                    $extracted = ABSPATH . $extracted;
                    if (file_exists($extracted)) {
                        unlink($extracted);
                    }

                    update_user_meta($user_ID, 'profile_pic_url', $fileurl);
                    update_user_meta($user_ID, 'thechamp_large_avatar', $fileurl);
                    $data = array('image' => $fileurl, 'message' => '<span class="text-success pic_msg1">Updated successfully...</span>');
                } else {
                    $data = array('image' => '', 'message' => '<span class="text-danger pic_msg1">Profile not uploaded..</span>');
                }
            }
        } else {
            $filename = get_user_meta($user_ID, 'profile_pic_url', TRUE);
            if ($filename == '') {
                update_user_meta($user_ID, 'profile_pic_url', '');
                update_user_meta($user_ID, 'thechamp_large_avatar', '');
                $data = array('image' => '', 'message' => '<span class="text-success pic_msg1">Updated successfully...</span>');
            } else {
                $data = array('image' => $filename, 'message' => '<span class="text-success pic_msg1">Updated successfully...</span>');
            }
        }
    }

    $update = update_user_meta($user_ID, 'first_name', $fname);
    $update = update_user_meta($user_ID, 'last_name', $lname);
    $update = update_user_meta($user_ID, 'zip_code', $zip_code);
    $update = update_user_meta($user_ID, 'state', $P_state);
    $update = update_user_meta($user_ID, 'address1', $add1);
    $update = update_user_meta($user_ID, 'address2', $add2);

    echo json_encode($data);
    die(0);
}

add_action('wp_ajax_nopriv_delete_acc', 'delete_acc_callback');
add_action('wp_ajax_delete_acc', 'delete_acc_callback');

function delete_acc_callback() {
    $user_id = $_POST['user_id'];
    echo wp_delete_user($user_id);
    die();
}

/* Ajax call for update basic information */
add_action('wp_ajax_nopriv_save_tax', 'save_tax_callback');
add_action('wp_ajax_save_tax', 'save_tax_callback');

function save_tax_callback() {

    global $wpdb;
    $user_id = get_current_user_id();

    $Is_Us_Person = $_POST["Is_Us_Person"];
    if ($Is_Us_Person == 0)
        $Is_Us_Person = '';
    $Business_Legal_Name = $_POST["Business_Legal_Name"];
    $federal_tax_classification = $_POST["federal_tax_classification"];
    $Is_SSN = $_POST["Is_SSN"];
    if ($Is_SSN == 0)
        $Is_SSN = '';
    $SSN_EIN = $_POST["SSN_EIN"];
    $Certify_under_penalties_perjury = $_POST["Certify_under_penalties_perjury"];
    if ($_POST["address"]) {
        $address = $_POST["address"];
    }


    $user = $wpdb->get_results("SELECT * FROM tbl_proofreader_general_details WHERE fk_proofreader_id = $user_id");
    $user_count = count($user);

    if ($user_count > 0) {
        if ($_POST["address"]) {
            $address = $_POST["address"];
            $result = $wpdb->update('tbl_proofreader_general_details', array(
                'Is_Us_Person' => $Is_Us_Person,
                'Business_Legal_Name' => $Business_Legal_Name,
                'Address' => $address,
                'federal_tax_classification' => $federal_tax_classification,
                'Is_SSN' => $Is_SSN,
                'SSN_EIN' => $SSN_EIN,
                'Certify_under_penalties_perjury' => $Certify_under_penalties_perjury), array('fk_proofreader_id' => $user_id));
        } else {
            $result = $wpdb->update('tbl_proofreader_general_details', array(
                'Is_Us_Person' => $Is_Us_Person,
                'Business_Legal_Name' => $Business_Legal_Name,
                'federal_tax_classification' => $federal_tax_classification,
                'Is_SSN' => $Is_SSN,
                'SSN_EIN' => $SSN_EIN,
                'Certify_under_penalties_perjury' => $Certify_under_penalties_perjury), array('fk_proofreader_id' => $user_id));
        }
        $result = 1;
    } else {
        if ($_POST["address"]) {
            $address = $_POST["address"];
            $result = $wpdb->insert('tbl_proofreader_general_details', array(
                'fk_proofreader_id' => $user_id,
                'Is_Us_Person' => $Is_Us_Person,
                'Business_Legal_Name' => $Business_Legal_Name,
                'Address' => $address,
                'federal_tax_classification' => $federal_tax_classification,
                'Is_SSN' => $Is_SSN,
                'SSN_EIN' => $SSN_EIN,
                'Certify_under_penalties_perjury' => $Certify_under_penalties_perjury
            ));
        } else {
            $result = $wpdb->insert('tbl_proofreader_general_details', array(
                'fk_proofreader_id' => $user_id,
                'Is_Us_Person' => $Is_Us_Person,
                'Business_Legal_Name' => $Business_Legal_Name,
                'federal_tax_classification' => $federal_tax_classification,
                'Is_SSN' => $Is_SSN,
                'SSN_EIN' => $SSN_EIN,
                'Certify_under_penalties_perjury' => $Certify_under_penalties_perjury
            ));
        }
    }

    echo $result;
    die();
}

/* Ajax call for update basic information */
add_action('wp_ajax_nopriv_save_paypal', 'save_paypal_callback');
add_action('wp_ajax_save_paypal', 'save_paypal_callback');

function save_paypal_callback() {
    global $wpdb;
    $user_id = get_current_user_id();
    $paypal_id = $_POST['paid_id'];

    $user = $wpdb->get_results("SELECT * FROM tbl_proofreader_general_details WHERE fk_proofreader_id = $user_id");
    $user_count = count($user);
    if ($user_count > 0) {
        $result = $wpdb->update('tbl_proofreader_general_details', array('Paypal_ID' => $paypal_id), array('fk_proofreader_id' => $user_id));
        $result = 1;
    } else {
        $result = $wpdb->insert('tbl_proofreader_general_details', array('fk_proofreader_id' => $user_id, 'Paypal_ID' => $paypal_id));
    }
    echo $result;
    die();
}

function ago($time) {
    $periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
    $lengths = array("60", "60", "24", "7", "4.35", "12", "10");

    $now = time();

    $difference = $now - $time;
    $tense = "ago";

    for ($j = 0; $difference >= $lengths[$j] && $j < count($lengths) - 1; $j++) {
        $difference /= $lengths[$j];
    }

    $difference = round($difference);

    if ($difference != 1) {
        $periods[$j] .= "s";
    }

    if ($now - $time < 15) {
        $result_vlaue = "Just Now";
    } else {
        $result_vlaue = $difference . ' ' . $periods[$j] . ' ago';
    }

    return $result_vlaue;
}

function date_getFullTimeDifference($start, $end) {
    $uts['start'] = strtotime($start);
    $uts['end'] = strtotime($end);
    if ($uts['start'] !== -1 && $uts['end'] !== -1) {
        if ($uts['end'] >= $uts['start']) {
            $diff = $uts['end'] - $uts['start'];

            if ($hours = intval((floor($diff / 3600))))
                $diff = $diff % 3600;
            if ($minutes = intval((floor($diff / 60))))
                $diff = $diff % 60;
            $diff = intval($diff);
            $return_str = $hours . ':' . $minutes . ':' . $diff;
            echo $return_str;
            //return( array('years' => $years, 'months' => $months, 'days' => $days, 'hours' => $hours, 'minutes' => $minutes, 'seconds' => $diff) );
        }
    }
}

//Remove Visual shortcode from page content
if (!function_exists('remove_vc_from_excerpt')) {

    function remove_vc_from_excerpt($excerpt) {
        $patterns = "/\[[\/]?vc_[^\]]*\]/";
        $replacements = "";
        return preg_replace($patterns, $replacements, $excerpt);
    }

}

/* bansi code */

add_action('wp_ajax_nopriv_save_proofreader_test', 'save_proofreader_test_callback');
add_action('wp_ajax_save_proofreader_test', 'save_proofreader_test_callback');

function save_proofreader_test_callback() {
    global $wpdb;

    $user_id = get_current_user_id();
    $desc = $_POST['word_desc'];
    $question = $_POST['test_question'];
    $post_index = $_POST['post_id'];

    $datetime = date('Y-m-d H:i:s');
    $timestamp = strtotime($datetime);
    $timer = $_POST['test_timer'];
    $test_index = $_POST['test_id'];
    $table_name = 'tbl_proofreader_test';

    if ($test_index == 5) {
        $subject = '[' . get_bloginfo('name') . '] Your application has been submitted';
        send_proof_notification('', $user_id, 'Your application has been submitted', 1, 1, $subject);
        update_user_meta($user_id, 'test_completed', TRUE);

        $user_info = get_userdata($user_id);

        /* Email to admin */
        $to = get_bloginfo('admin_email');
        $subject = '[' . get_bloginfo('name') . '] There is a new proofreader application';
        $msg = '';
        $msg .= 'There is a new proofreader application submitted: <br/><br/>';
        $msg .= 'Username: ' . $user_info->first_name . ' ' . $user_info->last_name . '<br/><br/>';
        $msg .= 'Email: ' . $user_info->user_email . '<br/><br/>';


        $html = '<div style="width: 850px; margin: 0 auto">
        <div style="width: 250px; margin: 0 auto">
        <a style="margin: 0 15px 0px 0; width: 250px" href="' . get_site_url() . '" onclick="return false" rel="noreferrer">
            <img src="' . of_get_option('header_logo') . '" alt="logo" style="width: 100%">                
        </a>    
        </div>
    <div style="background: #f2f0f1; padding: 20px; border-radius: 15px; margin: 20px 0; display: inline-block; width: 100%">            
        <div style="width: 100%; display: inline-block; margin-bottom: 30px">
            <h2 style="color: #0071bd; font-size: 13px; text-transform: capitalize;">Hi admin,</h2>
            <div >
                <span style=" color: #333; margin: 0; width: 100%; display: inline-block; padding: 8px 5px">
                    ' . $msg . '
                </span>
            </div>            
        </div>
        <div style="width: 100%; display: inline-block; margin-bottom: 30px">
            <h2 style="color: #0071bd; font-size: 13px;">Thank you!</h2>
            <div style="width: 100%; ">
                <span style=" color: #333; margin: 0; width: 100%; display: inline-block; padding: 0">
                   The ' . get_bloginfo() . ' Team
                </span>
            </div>
          </div>  
          <p style="font-size: 14px; color: #7c7c7c; line-height: 22px; text-align: center; ">' . of_get_option('copyright_text') . '</p>
    </div>
</div>';
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: ' . get_bloginfo() . ' <contact@writesaver.co>' . "\r\n";

        wp_mail($to, $subject, $html, $headers);
    }

    $insert = $wpdb->insert(
            $table_name, array(
        'fk_proofreader_id' => $user_id,
        'post_id' => $post_index,
        'test_id' => $test_index,
        'test_question' => $question,
        'test_submitted' => $desc,
        'created_date' => $datetime,
        'timing_used' => $timer,
            )
    );

    echo $insert;
    die(0);
}

function send_cust_notification($cust_id, $proof_id, $desc, $desk_noti = 1, $email_noti = 1, $subject = '', $noti = '') {

    $cust_info = get_userdata($cust_id);
    $proof_info = get_userdata($proof_id);

    $to = $cust_info->user_email;
    if ($email_noti == 1):
        if ($subject == '')
            $subject = $desc;
        Send_mail($cust_id, $to, $subject, $desc);
    endif;
    if ($desk_noti == 1):
        if ($noti == '')
            $noti = $desc;
        global $wpdb;
        $wpdb->insert(
                'tbl_customer_notifications', array(
            'notification_date' => date('Y-m-d H:i:s'),
            'fk_customer_id' => $cust_id,
            'fk_proofreader_id' => $proof_id,
            'description' => $noti
                )
        );
    endif;
}

function send_proof_notification($cust_id, $proof_id, $desc, $desk_noti = 1, $email_noti = 1, $subject = '', $noti = '') {

    $cust_info = get_userdata($cust_id);
    $proof_info = get_userdata($proof_id);

    $to = $proof_info->user_email;
    if ($subject == '')
        $subject = $desc;
    Send_mail($proof_id, $to, $subject, $desc);

    if ($desk_noti == 1):
        if ($noti == '')
            $noti = $desc;
        global $wpdb;
        $wpdb->insert(
                'tbl_proofreader_notifications', array(
            'notification_date' => date('Y-m-d H:i:s'),
            'fk_customer_id' => $cust_id,
            'fk_proofreader_id' => $proof_id,
            'description' => $noti
                )
        );
    endif;
}

add_action('wp_login', 'user_last_login', 0, 2);

function user_last_login($login, $user) {
    $user = get_user_by('login', $login);
    $now = date('Y-m-d');
    update_usermeta($user->ID, 'user_last_login', $now);
}

//remove the existing string/level cost text

function my_pmpro_level_cost_text($r, $level, $tags, $short) {
    $r = '';
    global $wpdb, $current_user;

    $orders = $wpdb->get_results("SELECT id FROM $wpdb->pmpro_membership_orders WHERE user_id =  $current_user->ID");

    if (count($orders) > 0)
        $InitialPayment = $level->billing_amount;
    else
        $InitialPayment = $level->initial_payment;  //initial payment
    if (!$short)
        $r = sprintf(__('The price for membership is <strong>%s</strong>', 'pmpro'), pmpro_formatPrice($InitialPayment));
    else
        $r = sprintf(__('%s', 'pmpro'), pmpro_formatPrice($level->billing_amount));
    return $r;
}

add_filter('pmpro_level_cost_text', 'my_pmpro_level_cost_text', 10, 4);


add_action('wp_ajax_nopriv_save_user_upload_doc123', 'save_user_upload_doc123_callback');
add_action('wp_ajax_save_user_upload_doc123', 'save_user_upload_doc123_callback');

function save_user_upload_doc123_callback() {
    $desc = $_POST['word_desc'];
    $desc = preg_replace('/\s+/', ' ', $desc);
    $break = explode(" ", $desc);
    $count = count($break);
//echo 'PHP COUNT == ' . $count;
//$totalNoOfWords = trim(str_word_count($desc));
//echo 'PHP COUNT = ' . $totalNoOfWords;


    $words_to_count = trim(strip_tags($desc));
    $pattern = "/[^(\w|\d|\'|\"|\.|\!|\?|;|,|\\|\/|\-\-|:|\&|@)]+/";
    $words_to_count = preg_replace($pattern, " ", trim($words_to_count));
    $words_to_count = trim($words_to_count);
    echo $words_to_count;

    $total_words = count(explode(" ", $words_to_count));
//echo 'PHP COUNT ===== ' . $count;

    die(0);
}

//test daily cron job

if (!wp_next_scheduled('wpb_custom_crons')) {
    wp_schedule_event(time(), 'daily', 'wpb_custom_crons');
}

add_action('wpb_custom_crons', 'wpb_custom_cron_funcs');

function wpb_custom_cron_funcs() {

    global $wpdb;
    
    $customerusers = get_users(array('role' => 'customer'));
    foreach ($customerusers as $cuser) {
        $user_id = $cuser->ID;

        $document_info = $wpdb->get_results("SELECT * FROM wp_customer_document_main WHERE fk_customer_id = $user_id AND Status=1  ORDER BY pk_document_id ASC LIMIT 1");
        $plan_info = $wpdb->get_results("SELECT * FROM wp_pmpro_membership_orders WHERE user_id = $user_id  ORDER BY id ASC LIMIT 1");
        $current_date = strtotime(date('Y-m-d'));

        if (!empty($document_info) && empty($plan_info)) {

            $first_pur_date = $document_info[0]->created_date;
            $first_pur_date = date("Y-m-d", strtotime($first_pur_date));
            $first_pur_date = strtotime($first_pur_date);

            $date_diff = ($current_date - $first_pur_date);
            $diff_day = round($date_diff / 86400);
            if ($diff_day == 2 || $diff_day == 7) {
            if ($diff_day == 2) {
                $cust_info = get_userdata($user_id);
                $to = $cust_info->user_email;
                $subject = 'Thanks for checking out our free trial! We\'d love your feedback';
                $desc = "Glad you got a chance to try out our free trial! We're a relatively new company and would love your feedback on our service. If you could reply to this email with feedback on your experience, we'd really appreciate it!";
            }

            if ($diff_day == 7) {
                $cust_info = get_userdata($user_id);
                $to = $cust_info->user_email;
                $subject = 'Quick Question for you';
                $desc = "Hope you enjoyed your free words with Writesaver! We'd love to hear about your experience, shoot us an email and let us know about your experience with Writesaver.";
            }
                        Send_mail($user_id, $to, $subject, $desc);
        }
        }

        $user_registered = date("Y-m-d", strtotime($cuser->user_registered));
        $user_registered = strtotime($user_registered);

        $diff = $current_date - $user_registered;
        $day = round($diff / 86400);

        //$user_info = $wpdb->get_results("SELECT * FROM tbl_customer_general_info WHERE fk_customer_id = $user_id LIMIT 1");
        $user_info = $wpdb->get_results("SELECT * FROM wp_customer_document_main WHERE fk_customer_id = $user_id ");

        $free_words = $user_info[0]->free_words;
        $total_free_words = of_get_option('free_words_for_customer');

        //if ($total_free_words == $free_words) {
        if (count($user_info) == 0) {
            if ($day == 1 || $day == 7) {
                if ($day == 1) {
                    $subject = 'Writesaver Free Trial Opportunity';
                    $descs = "We just wanted to make sure you saw our free trial opportunity for Writesaver. We have native English speaking proofreaders standing by waiting to edit your emails, papers, and documents. 

<br/><br/><a href=https://www.writesaver.co>Click Here to Get Started</a>
<br/><br/>
If you have any questions about our service prior to getting started you can just reply to this email with any questions, we'll be happy to help you!";
                    $noti = "What are you waiting for? Your first 500 words is free!";
                }

                if ($day == 7) {
                    $subject = 'Quick Question for You';
                    $descs = "
About a week ago you signed up for Writesaver, but we noticed you haven't yet given your free trial a test run yet! We wanted to know why so we can further improve our service for you. Shoot us an email and let us know what we could do better!";
                    $noti = "Give your free trial a test run today and start writing with perfect English!";
                }
                //function send_cust_notification($cust_id, $proof_id, $desc, $desk_noti = 1, $email_noti = 1, $subject = '', $noti = '')
                send_cust_notification($user_id, '', $descs, 1, 1, $subject, $noti);
            }
        }
    }

    $blogusers = get_users(array('role' => 'proofreader'));
// Array of stdClass objects.
    foreach ($blogusers as $user) {

        $user_id = $user->ID;

//Login notification
        $last_login = get_user_meta($user_id, 'user_last_login', TRUE);
        $last_login = date("Y-m-d", strtotime($last_login));
        $last_login = strtotime($last_login);
        $current_date = strtotime(date('Y-m-d'));
        $diffs = $current_date - $last_login;
        $diffs = round($diff / 86400);
        if ($diffs == 5 || $diffs == 12 || $diffs == 20 || $diffs == 35) {

            if ($diffs == 5) {
                $subject = "Your Writesaver Experience";
                $descs = "
We noticed you haven't been around for a while, and wanted to make sure you're having a great experience as a Writesaver proofreader. If you've just gotten busy and haven't had time to log-in, that's totally fine. One of the best things about Writesaver is that you can do it on your own schedule. But if there's something we can do to make your proofreading experience better, please let us know by responding to this email. We'll take your feedback into consideration and get back to you.";
                $descs .=  "<br/><br/><strong><em><a href='" . get_permalink(810) . "' >Access your proofreader dashboard</a></em></strong>";
            }
            if ($diffs == 12) {
                $subject = "Your feedback on Writesaver so far";
                $descs = "Just wanted to check in and make sure you're still having a great experience as a Writesaver proofreader. Let us know if there's anything we need to improve to make your experience better! We look forward to hearing from you!";
                $descs .=  "<br/><br/><strong><em><a href='" . get_permalink(810) . "' >Access your proofreader dashboard</a></em></strong>";
                
            }
            if ($diffs == 20) {
                $subject = 'Just Checking In';
                $descs = "We've noticed you've gone a little while without checking into Writesaver, and we wanted to make sure you're still happy with your experience with Writesaver! Shoot us an email when you can, we'd love to hear from you! If you've just been short on time, you can access your proofreader dashboard whenever you're ready to start editing again by clicking the button below:";
                $descs .=  "<br/><br/><strong><em><a href='" . get_permalink(810) . "' >Access your proofreader dashboard</a></em></strong>";

            }
            /*
            if ($diffs == 35) {
                $subject = 'Quick Question...';
                $descs = 'Time to restart your proofreading';
            }
            */
            send_proof_notification('', $user_id, $descs, 1, 1, $subject);
        }

//Complete test notification
        $test1_result = $wpdb->get_results("SELECT *  FROM tbl_proofreader_test WHERE fk_proofreader_id = $user_id AND test_id=1 ");
        $test5_result = $wpdb->get_results("SELECT *  FROM tbl_proofreader_test WHERE fk_proofreader_id = $user_id AND test_id=5 ");

        if (!empty($test1_result) && empty($test5_result)) {

            $start_date = date("Y-m-d", strtotime($test1_result[0]->created_date));
            $start_date = strtotime($start_date);
            $current_date = strtotime(date('Y-m-d'));
            $diff = $current_date - $start_date;
            $day = round($diff / 86400);

            if ($day == 2) {
                $subject = 'Your Incomplete Writesaver Application';
                $descs = "We're glad to see you started an application to be a proofreader for Writesaver, but noticed you haven't finished it yet! To complete your application, <em><strong><a href=\"http://www.writesaver.co\">Click Here</a></strong></em>. If you have any questions about the application process, feel free to send us an email!";
                send_proof_notification('', $user_id, $descs, 1, 1, $subject);
            }
        }


//Document avalilability notification
        $current_date = strtotime(date('Y-m-d'));
        $results = $wpdb->get_results("SELECT * FROM wp_assigned_document_details WHERE fk_proofreader_id= $user_id AND status='Pending' ");
        if ($results) {
            foreach ($results as $result) {
                $assign_date = $result->assign_date;
                $assign_date = date("Y-m-d", strtotime($assign_date));
                $assign_date = strtotime($assign_date);
                $diffss = $current_date - $assign_date;
                $days = round($diffss / 86400);

                if ($days == 2) {
                    $result_user = $wpdb->get_results("SELECT * FROM wp_proofreader_notification_setting WHERE user_id= $user_id");
                    $desk_noti = $result_user[0]->desktop_other_notification;
                    $email_noti = $result_user[0]->email_info_about_availability;
                    $subject = '[' . get_bloginfo('name') . '] Document is available for proofreading.';
                    $descs = "Document is available for proofreading.";
                    send_proof_notification('', $user_id, $descs, $desk_noti, $email_noti, $subject);
                }
            }
        }
    }
}

//add script for validation of word Price for proofreader in theme option

function of_repeat_script() {
    ?>
    <script type="text/javascript">
        jQuery(function ($) {
            $("#proofreader_word_price, #popup_select_time").keypress(function (e) {
                var a = [46];
                var k = e.which;
                for (i = 48; i < 58; i++)
                    a.push(i);

                if (!(jQuery.inArray(k, a) >= 0))
                    e.preventDefault();
            });
        });
    </script>
    <?php
}

add_action('optionsframework_custom_scripts', 'of_repeat_script');

add_action('show_user_profile', 'extra_user_profile_fields');
add_action('edit_user_profile', 'extra_user_profile_fields');

function extra_user_profile_fields($user) {
    ?>

    <table class="form-table">       
        <tr>
            <th><label for="postalcode"><?php _e("Phone Number"); ?></label></th>
            <td>
                <input type="text" name="phone_no" id="phone_no" value="<?php echo esc_attr(get_the_author_meta('_phone_number', $user->ID)); ?>" class="regular-text" /><br />
            </td>
        </tr>
    </table>
    <?php
}

add_action('personal_options_update', 'save_extra_user_profile_fields');
add_action('edit_user_profile_update', 'save_extra_user_profile_fields');

function save_extra_user_profile_fields($user_id) {

    if (!current_user_can('edit_user', $user_id)) {
        return false;
    }

    update_user_meta($user_id, '_phone_number', $_POST['phone_no']);
}

//Proofreader send payment request
add_action('wp_ajax_nopriv_proofreader_request_payment', 'proofreader_request_payment_callback');
add_action('wp_ajax_proofreader_request_payment', 'proofreader_request_payment_callback');

function proofreader_request_payment_callback() {
    global $wpdb;
    $user_ID = get_current_user_id();

    $subject = "[" . get_bloginfo('name') . "] We've received and are processing your payment request.";
    $descs = "<p>We've received your payment request. It can take up to 3 days to process your request, we'll make sure you get paid as soon as possible!<p>";
    $noti = "<p>We've received your payment request. It can take up to 3 days to process your request, we'll make sure you get paid as soon as possible!<p>";
    $descs .= "<p></br></br> Your document details are displayed below:</p>";


    $doc_revenuess = $wpdb->get_results("SELECT * FROM tbl_proofreader_revenue WHERE fk_proofreader_id= $user_ID AND status = 'Pending'");
    if ($doc_revenuess) {

        $doc_table .= '<table border="1">';
        $doc_table .= '<thead>';
        $doc_table .= '<tr>';
        $doc_table .= ' <th>Doc Title</th>';
        $doc_table .= '<th>Section No.</th>';
        $doc_table .= '<th>Requested Amount</th>';
        $doc_table .= ' <th>Requested Date</th>';
        $doc_table .= ' <th>Edited Word</th>';
        $doc_table .= ' <th>Payment Status</th>';
        $doc_table .= ' <th>Check Status</th>';
        $doc_table .= '</tr>';
        $doc_table .= '</thead>';
        $doc_table .= '<tbody>';

        foreach ($doc_revenuess as $doc_revenue) {
            $doc_datail_id = $doc_revenue->fk_doc_id;
            $check_proof = $wpdb->get_row("SELECT * FROM `tbl_proofreaded_doc_details`  Where fk_doc_details_id =  $doc_datail_id");
            if ($check_proof->fk_proofreader_id == $user_ID) {
                $cstatus = "Single Check";
            } elseif ($check_proof->Fk_DoubleProofReader_Id == $user_ID) {
                $cstatus = "Double Check";
            } else {
                $cstatus = "";
            }
            $main_doc = $wpdb->get_row("SELECT * FROM wp_customer_document_details WHERE pk_doc_details_id= $doc_datail_id AND is_active= 1");

            $result_maindoc = $wpdb->get_row("SELECT * FROM wp_customer_document_main WHERE pk_document_id= $main_doc->fk_doc_main_id AND Status=1");
            $total_docs = $wpdb->get_results("SELECT * FROM wp_customer_document_details WHERE fk_doc_main_id= $result_maindoc->pk_document_id AND is_active= 1 ORDER BY pk_doc_details_id");
            $count = 0;
            foreach ($total_docs as $total_doc) {
                $count++;
                if ($total_doc->pk_doc_details_id == $doc_datail_id) {
                    $doc_count = $count;
                }
            }

            $proof_info = get_userdata($user_ID);

            $doc_table .= '<tr>';
            $doc_table .= '<td>' . $result_maindoc->document_title . '</td>';
            $doc_table .= '<td>' . $doc_count . '</td>';
            $doc_table .= '<td>$' . $doc_revenue->requested_amount . '</td>';
            $doc_table .= '<td>' . date("m/d/Y h:i:s A") . '</td>';
            $doc_table .= '<td>' . $doc_revenue->edited_word . '</td>';
            $doc_table .= '<td>Process</td>';
            $doc_table .= '<td>' . $cstatus . '</td>';
            $doc_table .= '</tr>';
        }
        $doc_table .= '</tbody>';
        $doc_table .= '</table>';

        $descs = $descs . ' ' . $doc_table;
        send_proof_notification('', $user_ID, $descs, 1, 1, $subject, $noti);


        $site_admin_email = get_bloginfo('admin_email');
        $subject = 'Payment request received from ' . $proof_info->first_name . ' ' . $proof_info->last_name;
        $desc = "<p>Payment request received from " . $proof_info->first_name . " " . $proof_info->last_name;
        $desc .= "<p></br></br>Document details displayed below:</p>";
        $desc = $desc . ' ' . $doc_table;
        Send_mail('', $site_admin_email, $subject, $desc);
    }

    echo $wpdb->update(
            'tbl_proofreader_revenue', array('status' => 'Process', 'requested_date' => date('Y-m-d H:i:s'), "modified_date" => date('Y-m-d H:i:s')), array('status' => 'Pending', 'fk_proofreader_id' => $user_ID)
    );



    die();
}

function filter_plugin_updates($value) {
    unset($value->response['super-socializer/super_socializer.php']);
    unset($value->response['newsletter/plugin.php']);
    unset($value->response['paid-memberships-pro/paid-memberships-pro.php']);
    return $value;
}

add_filter('site_transient_update_plugins', 'filter_plugin_updates');

//add view user detail column to user list in admin
add_filter('manage_users_columns', 'pippin_add_user_id_column');

function pippin_add_user_id_column($columns) {

    $columns['view_user'] = 'View';
    $columns['user_status'] = 'User Status';
    return $columns;
}

add_action('manage_users_custom_column', 'pippin_show_user_id_column_content', 10, 3);

function pippin_show_user_id_column_content($value, $column_name, $user_id) {
    $user = get_userdata($user_id);

    if ('view_user' == $column_name) {
        if ($user->roles[0] != administrator) {
            return '<a href="' . site_url() . '/wp-admin/admin.php?page=view_user&user=' . $user_id . '"><i class="fa fa-eye" aria-hidden="true"></i></a>';
        }
    }
    if ('user_status' == $column_name)
        if ($user->roles[0] == proofreader) {
            $info = get_user_meta($user_id, 'info_completed', true);
            $test = get_user_meta($user_id, 'test_completed', true);
            $test_status = get_user_meta($user_id, 'test_status', TRUE);

            if ($info != 1)
                $status = "You have not completed your profile information yet.";
            elseif ($test != 1)
                $status = "You haven't completed all your tests yet.";
            elseif ($info == 1 && $test == 1 && $test_status == '')
                $status = "Our team is reviewing your application, and will get back to you soon!";
            elseif ($test_status == 'accepted')
                $status = "Accepted";
            elseif ($test_status == 'delayed')
                $status = "Delayed";
            return '<p>' . $status . '<p>';
        }
    return $value;
}

//Email template
function Send_mail($user_id, $to, $subject, $desc) {

    if ($user_id)
        $user_info = get_userdata($user_id);
    $user_roles_array = $user_info->roles;
    $user_role = array_shift($user_roles_array);
    if ($user_role == 'customer') {
        $setting_link = get_the_permalink(754);
    } elseif ($user_role == 'proofreader') {
        $setting_link = get_the_permalink(768);
    } else {
        $setting_link = '';
    }
    $msg = '<div style="width: 850px; margin: 0 auto">
            <div style="width: 250px; margin: 0 auto">
            <a style="margin: 0 15px 0px 0; width: 250px" href="' . get_site_url() . '" onclick="return false" rel="noreferrer">
                <img src="' . of_get_option('header_logo') . '" alt="logo" style="width: 100%">                
            </a>    
            </div>
        <div style="background: #f2f0f1; padding: 20px; border-radius: 15px; margin: 20px 0; display: inline-block; width: 100%">            
            <div style="width: 100%; display: inline-block; margin-bottom: 30px">';
    if ($user_id)
        $msg .= ' <h2 style="color: #0071bd; font-size: 13px; text-transform: capitalize;">Hi ' . $user_info->first_name . ' ' . $user_info->last_name . ',</h2>';
    else
        $msg .= ' <h2 style="color: #0071bd; font-size: 13px; text-transform: capitalize;">Hi admin,</h2>';
    $msg .= ' <div >
                    <div style=" color: #333; margin: 0; width: 100%; display: inline-block; padding: 8px 5px">
                        ' . $desc . '
                    </div>
                </div>            
            </div>
            <div style="width: 100%; display: inline-block; margin-bottom: 30px">
                <h2 style="color: #0071bd; font-size: 13px;">Thank you!</h2>
                <div style="width: 100%; ">
                    <span style=" color: #333; margin: 0; width: 100%; display: inline-block; padding: 0">
                        The ' . get_bloginfo() . ' Team
                    </span>
                </div>
              </div>  
              <p style="font-size: 14px; color: #7c7c7c; line-height: 22px; text-align: center; ">' . of_get_option('copyright_text') . '</p>';

    if ($setting_link)
        $msg .= ' <p style="font-size: 14px;  line-height: 22px; text-align: center; ">To change your notification settings  <a href="' . $setting_link . '">click here.</a></p>';
    $msg .= ' </div>
    </div>';
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
    $headers .= 'From: ' . get_bloginfo() . ' <contact@writesaver.co>' . "\r\n";
    wp_mail($to, $subject, $msg, $headers);
    //wp_mail("tujazujume@lucyu.com", $subject, $msg, $headers);
}

/* ------------------- Reset Password Email --------------------------------- */

function reset_pw_ajax() {
    $pass = $_POST['oldpass'];
    $password = $_POST['password'];
    $user_id = $_POST['user_id'];
    $email = $_POST['email'];

    if ($password != '' && $user_id != '' && $email != '' && $pass != $password) {
        wp_set_password($password, $user_id);
        $to = $email;
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'This is my secret key';
        $secret_iv = 'This is my secret iv';
// hash
        $key = hash('sha256', $secret_key);

// iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        $output = openssl_encrypt($email, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);

         $subject = 'Your Password Has Been Reset';
        $msg = 'Your password has been reset successfully. <br/><br/>';
        //$msg .= 'Password: ' . $password . '<br/><br/>';
        $msg .= '<a href=" ' . get_permalink(545) . '">Login Now</a>';
        $user_info = get_user_by_email($email);

        $html = '<div style="width: 850px; margin: 0 auto">
        <div style="width: 250px; margin: 0 auto">
        <a style="margin: 0 15px 0px 0; width: 250px" href="' . get_site_url() . '" onclick="return false" rel="noreferrer">
            <img src="' . of_get_option('header_logo') . '" alt="logo" style="width: 100%">                
        </a>    
        </div>
    <div style="background: #f2f0f1; padding: 20px; border-radius: 15px; margin: 20px 0; display: inline-block; width: 100%">            
        <div style="width: 100%; display: inline-block; margin-bottom: 30px">
            <h2 style="color: #0071bd; font-size: 13px; text-transform: capitalize;">Hi ' . $user_info->first_name . ' ' . $user_info->last_name . ',</h2>
            <div >
                <span style=" color: #333; margin: 0; width: 100%; display: inline-block; padding: 8px 5px">
                    ' . $msg . '
                </span>
            </div>            
        </div>
        <div style="width: 100%; display: inline-block; margin-bottom: 30px">
            <h2 style="color: #0071bd; font-size: 13px;">Thank you!</h2>
            <div style="width: 100%; ">
                <span style=" color: #333; margin: 0; width: 100%; display: inline-block; padding: 0">
                    The ' . get_bloginfo() . ' Team
                </span>
            </div>
          </div>  
          <p style="font-size: 14px; color: #7c7c7c; line-height: 22px; text-align: center; ">' . of_get_option('copyright_text') . '</p>
    </div>
</div>';
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: ' . get_bloginfo() . ' <contact@writesaver.co>' . "\r\n";

        wp_mail($to, $subject, $html, $headers);
        echo '1';
    } else {
        echo '0';
    }
    die();
}

add_action('wp_ajax_nopriv_reset_pw_ajax', 'reset_pw_ajax');
add_action('wp_ajax_reset_pw_ajax', 'reset_pw_ajax');
/* ------------------- End Reset Password Email --------------------------------- */


/* ------------- Check Duplicate Document Name on Customer upload document --------------- */
add_action('wp_ajax_nopriv_check_doc_name', 'check_doc_name_callback');
add_action('wp_ajax_check_doc_name', 'check_doc_name_callback');

function check_doc_name_callback() {
    global $wpdb;
    $user_ID = get_current_user_id();
    $DocumentTitle = $_POST['DocumentTitle'];
    $doc_count = 0;
    $documents = $wpdb->get_results("SELECT document_title FROM `wp_customer_document_main`  where fk_customer_id =  $user_ID AND Status=1");
    foreach ($documents as $document) {
        if ($document->document_title == $DocumentTitle) {
            $doc_count++;
        }
    }
    echo $doc_count;
    die();
}

//Count word 
function get_word_count($sentence) {
    $break = explode(" ", $sentence);
    $count = count($break);
    return $count;
}

// Encrypt function /

function encrypt_string($string) {

    $encrypt_method = "AES-256-CBC";
    $secret_key = 'This is my secret key';
    $secret_iv = 'This is my secret iv';

// hash
    $key = hash('sha256', $secret_key);

// iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);

    $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
    return base64_encode($output);
}

// End Encrypt function /
// Decrypt function /

function decrypt_string($string) {
    $encrypt_method = "AES-256-CBC";
    $secret_key = 'This is my secret key';
    $secret_iv = 'This is my secret iv';

// hash
    $key = hash('sha256', $secret_key);
// iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);

    return openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
}

// End Decrypt function /
