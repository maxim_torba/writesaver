<?php

/*
 * Template Name: Stripe Webhook
 */


global $wpdb;


$stripe_secretkey = pmpro_getOption("stripe_secretkey");

require_once 'stripe/Stripe.php';
Stripe::setApiKey($stripe_secretkey);

$body = @file_get_contents('php://input');
$event_json = json_decode($body);
file_put_contents('stripe_request.txt', print_r($event_json, true) . PHP_EOL, FILE_APPEND | LOCK_EX);

if ($event_json->type == 'invoice.payment_succeeded') {

    $subscription_transaction_id = $event_json->data->object->lines->data[0]->id;
    $code = $event_json->data->object->lines->data[0]->plan->id;
    $price = $event_json->data->object->lines->data[0]->amount / 100;
    $stripe_reference = $event_json->data->object->charge;
    $attempt_count = $event_json->data->object->attempt_count;

    if ($attempt_count > 0) {

        $order_info = $wpdb->get_row("SELECT * FROM wp_pmpro_membership_orders WHERE code = '$code' and subscription_transaction_id = '$subscription_transaction_id'");

        if ($order_info) {

            $user_id = $order_info->user_id;
            $level_id = $order_info->membership_id;
            $order_detail = pmpro_getLevel($level_id);
            $plan_words = $order_detail->plan_words;

            $user_info = $wpdb->get_results("SELECT * FROM tbl_customer_general_info WHERE fk_customer_id = $user_id LIMIT 1");

            if (!empty($user_info)) {

                $remaining_credit_words = $user_info[0]->remaining_credit_words;
                $total_worls = $remaining_credit_words + $plan_words;
                $info_id = $user_info[0]->pk_customer_general_id;
                $wpdb->update(
                        'tbl_customer_general_info', array('remaining_credit_words' => $total_worls), array('pk_customer_general_id' => $info_id), array('%d'), array('%d')
                );

                $date = date('Y-m-d h:i:s');
                $description = "Subscriptions charges for " . $order_detail->name;
                $wpdb->insert('wp_price_per_extra_words', array('fk_customer_id' => $user_id, 'stripe_reference' => $stripe_reference, 'payment_date' => $date, 'price' => $price, 'descriptions' => $description, 'payment_source' => 'Stripe', 'words' => $plan_words, 'status' => 1));
            }
        }
    }
}

if ($event_json->type == 'customer.subscription.deleted') {

    $subscription_transaction_id = $event_json->data->object->id;
    $code = $event_json->data->object->items->data[0]->plan->id;
    $order_info = $wpdb->get_row("SELECT * FROM wp_pmpro_membership_orders WHERE code = '$code' and subscription_transaction_id = '$subscription_transaction_id'");

    if ($order_info) {

        $user_id = $order_info->user_id;
        $level_id = $order_info->membership_id;
        $user = get_userdata($user_id);
        pmpro_cancelMembershipLevel($level_id, $user_id, 'cancelled');

        //send an email to the member
        $myemail = new PMProEmail();
        $myemail->sendCancelEmail($user, $level_id);

        //send an email to the admin
        $myemail = new PMProEmail();
        $myemail->sendCancelAdminEmail($user, $level_id);
    }
}
?>
