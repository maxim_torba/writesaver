<?php

/*
 * Template Name: paypal-process
 */

$plan_id = $_REQUEST['level'];

$success = home_url() . "/paypal-process/";
$cancel = get_the_permalink(762) . "/?token=xd0eu8c9cxd&level=cancel";

$plan_dtls = $wpdb->get_results("SELECT * FROM wp_pmpro_membership_levels WHERE id = " . $plan_id);

//echo '<pre>';
//print_r($plan_dtls);
//exit;

$item = $plan_dtls[0]->name . ' at Writesaver';

$amount = round($plan_dtls[0]->billing_amount, 2);

//no of month,days or years
$period = $plan_dtls[0]->cycle_number;
global $period_length;
//M-month,D-Day,Y-year
if ($plan_dtls[0]->cycle_period == 'Month') {
    $period_length = 'M';
} else if ($plan_dtls[0]->cycle_period == 'Day') {
    $period_length = 'D';
} else if ($plan_dtls[0]->cycle_period == 'Year') {
    $period_length = 'Y';
}

//store card data
global $wpdb;


$user_id = get_current_user_id();
$user_info = get_userdata($user_id);
$prefix = $wpdb->prefix;
$table_name = $prefix . 'creditdebit_card_details';
$user_infio = $wpdb->get_row("SELECT * FROM $table_name WHERE customer_id = $user_id");
$ExpMonth = $user_infio->expirymonth;
$CCNumber = decrypt_string($user_infio->cardnumber);
$ExpYear = $user_infio->expyear;
$CVNumber = $user_infio->securitycode;

$gen_info = $wpdb->get_row("SELECT * FROM  tbl_customer_general_info WHERE fk_customer_id = $user_id");



$CardType = "Visa";

$postalcode = "751010";

######################################################################
######################## PAYPAL ######################################
######################################################################
require_once('paypal.class.php');  // include the class file	

$p = new paypal_class;             // initiate an instance of the class
$business_emails = $gen_info->paypal_id;

$business_email = get_option('pmpro_gateway_email');
//$business_email = 'suresk_1314104870_biz@yahoo.com';

$gateway_environment = get_option('pmpro_gateway_environment');
if ($gateway_environment == 'sandbox') {
    $p->paypal_url = "https://www.sandbox.paypal.com/cgi-bin/webscr";
} elseif ($gateway_environment == 'live') {
    $p->paypal_url = "https://www.paypal.com/cgi-bin/webscr";
}

$currency_code = get_option('pmpro_currency');

//$p->paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr'; // Testing paypal url
// if there is not action variable, set the default action of 'process'
if (empty($_GET['action']))
    $_GET['action'] = 'process';

switch ($_GET['action']) {
    case 'process':      // Process and order...	
        //card details variables
        $p->add_field('first_name', $user_info->first_name);
        $p->add_field('last_name', $user_info->last_name);
        $p->add_field('credit_card_type', $CardType);
        $p->add_field('cc_number', $CCNumber);
        $p->add_field('cvv2_number', $CVNumber);
        $p->add_field('expdate_month', $ExpMonth);
        $p->add_field('expdate_year', $ExpYear);
        $p->add_field('email', $business_emails); //
        $p->add_field('login_email', $business_emails);

        $p->add_field('zip', $postalcode);
        //paypal process variables
        $p->add_field('business', $business_email); //
        $p->add_field('cmd', '_xclick-subscriptions');
        $p->add_field('return', $success . '?action=success');
        $p->add_field('cancel_return', $success . '?action=cancel');
        $p->add_field('currency_code', $currency_code);
        $p->add_field('item_name', $item);
        $p->add_field('item_number', $plan_id);

        //$p->add_field('custom', $user_id);		 
        //subscription variables
        $p->add_field('a3', $amount);
        $p->add_field('p3', $period);
        $p->add_field('t3', $period_length);
        $p->add_field("src", 1); //Subscribe will start
        $p->add_field("sra", 1); //Reattemp to pay if failure	
        $p->add_field('no_note', '1');
        $p->submit_paypal_post(); // submit the fields to paypal
        //$p->dump_fields();      // for debugging, output a table of all the fields
        break;
    case 'success':      // Order was successful...

        $order = new MemberOrder();

        $plan_dtls = $wpdb->get_results("SELECT * FROM wp_pmpro_membership_levels WHERE id = " . $_REQUEST['item_number']);

        $plan_words = 0;
        if ($plan_dtls[0]->plan_words > 0) {
            $plan_words = $plan_dtls[0]->plan_words;
        }

        $user_id = get_current_user_id();

       $str = "INSERT INTO wp_pmpro_membership_orders set `code`='" . $order->getRandomCode() . "', `session_id`='" . session_id() . "', `user_id`='" . $user_id . "', `membership_id`='" . $_REQUEST['item_number'] . "', `paypal_token`='" . $_REQUEST['payer_id'] . "', `billing_name`='', `billing_street`='', `billing_city`='', `billing_state`='', `billing_zip`='', `billing_country`='', `billing_phone`='', `subtotal`='" . $_REQUEST['amount3'] . "', `tax`=0, `couponamount`='', `certificate_id`='', `certificateamount`='', `total`='" . $_REQUEST['amount3'] . "', `payment_type`='PayPal Express', `cardtype`='', `accountnumber`='', `expirationmonth`='', `expirationyear`='', `status`='success', `gateway`='paypalexpress', `gateway_environment`='" . $gateway_environment . "', `payment_transaction_id`='" . $_REQUEST['subscr_id'] . "', `subscription_transaction_id`='" . $_REQUEST['subscr_id'] . "', `timestamp`=now(), `affiliate_id`='', `affiliate_subid`='', `notes`='', `checkout_id`=''";
        $wpdb->query($str);

        $update = "update tbl_customer_general_info set remaining_credit_words=remaining_credit_words+" . $plan_words . " where fk_customer_id ='" . $user_id . "'";
        $wpdb->query($update);

        /* Adding Subscription for user */
        $startdate = current_time("mysql");
        $plan_id = $_REQUEST['item_number'];
        $pmpro_level = pmpro_getLevel($plan_id);
        //calculate the end date
        if (!empty($pmpro_level->expiration_number)) {
            $enddate = date_i18n("Y-m-d", strtotime("+ " . $pmpro_level->expiration_number . " " . $pmpro_level->expiration_period, current_time("timestamp")));
        } else {
            $enddate = "NULL";
        }
        $custom_level = array(
            'user_id' => $user_id,
            'membership_id' => $pmpro_level->id,
            'code_id' => "",
            'initial_payment' => $pmpro_level->initial_payment,
            'billing_amount' => $pmpro_level->billing_amount,
            'cycle_number' => $pmpro_level->cycle_number,
            'cycle_period' => $pmpro_level->cycle_period,
            'billing_limit' => $pmpro_level->billing_limit,
            'trial_amount' => $pmpro_level->trial_amount,
            'trial_limit' => $pmpro_level->trial_limit,
            'startdate' => $startdate,
            'enddate' => $enddate
        );
        pmpro_changeMembershipLevel($custom_level, $user_id, 'active');
        //update the current user
        global $current_user;
        if (!$current_user->ID && $user->ID) {
            $current_user = $user;
        } //in case the user just signed up
        pmpro_set_current_user();
        echo "<pre>";
        print_r($_REQUEST);

        /* $custom=array();
          //Write into the file
          $fp=fopen("hellotest.txt","w");
          foreach($_POST as $key => $value){
          fwrite($fp,$key.'===='.$value."\n");
          } */
        // This is where you would probably want to thank the user for their order
        // or what have you.  The order information at this point is in POST 
        // variables.  However, you don't want to "process" the order until you
        // get validation from the IPN.  That's where you would have the code to
        // email an admin, update the database with payment status, activate a
        // membership, etc.  	 
        //header("location:thanks_payment.php");
         header("location:" . get_the_permalink(762) . "?token=xs00u8c9cxd&level=success");
        exit;

        // You could also simply re-direct them to another page, or your own 
        // order status page which presents the user with the status of their
        // order based on a database (which can be modified with the IPN code 
        // below).
        break;
    case 'cancel':       // Order was canceled...	
        // The order was canceled before being completed.			
        //header("location:index.php");
        header("location:" . $cancel);
        break;
    case 'ipn':          // Paypal is calling page for IPN validation...	   
        // It's important to remember that paypal calling this script.  There
        // is no output here.  This is where you validate the IPN data and if it's
        // valid, update your database to signify that the user has payed.  If
        // you try and use an echo or printf function here it's not going to do you
        // a bit of good.  This is on the "backend".  That is why, by default, the
        // class logs all IPN data to a text file.		  
        if ($p->validate_ipn()) {

            extract($_POST);
            if ($txn_id <> '') {
                $custom = array();
                //Write into the file
                $fp = fopen("hellotest.txt", "w");
                foreach ($_POST as $key => $value) {
                    fwrite($fp, $key . '====' . $value . "\n");
                }
                //$custom=explode("::",$_POST[custom]);
            }
        }
        break;
}
?>
        