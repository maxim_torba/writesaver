<?php
/*
 * Template Name:  Proofreader Notification
 */
get_header();
global $wpdb;
$current_user = wp_get_current_user();
$user_roles_array = $current_user->roles;
$user_role = array_shift($user_roles_array);
if (!is_user_logged_in() || $user_role != "proofreader") {
    echo '<script>window.location.href="' . get_site_url() . '"</script>';
    exit;
}

$user_id = get_current_user_id();
$customer_info = $wpdb->get_results(" SELECT * FROM `tbl_proofreader_notifications` WHERE fk_proofreader_id = $user_id");
?>
<script>
    $(document).ready(function () {
        $.ajax({
            type: "POST",
            url: "<?php echo admin_url('admin-ajax.php'); ?>",
            data: {
                action: "read_notification"
            },
            success: function (data) {
                $('ul.notify_list').html(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
            }
        });
        return true;
    });
</script>
<section>
    <div class="breadcum">
        <div class="container">
            <div class="page_title">
                <h1>Notifications</h1>
            </div>
        </div>
    </div>
</section>        

<section>
    <div class="container">
        <div class="privacy notification">
            <div class="notify_content">
                <ul>
                    <?php
                    $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
                    $args = array(
                        'total_items' => $total,
                        'per_page' => $limit,
                        'total_pages' => $num_of_pages
                    );
                    $the_query = new WP_Query($args);
                    $query = "SELECT COUNT(fk_proofreader_id) FROM `tbl_proofreader_notifications` WHERE fk_proofreader_id= $user_id ";
                    $total = $wpdb->get_var($query);
                    $limit = 10;
                    $offset = ( $paged - 1 ) * $limit;
                    $num_of_pages = ceil($total / $limit);
                    $notification_list = $wpdb->get_results("SELECT * FROM `tbl_proofreader_notifications` WHERE fk_proofreader_id= $user_id ORDER BY pk_proof_notification_id DESC LIMIT $offset, $limit");

                    if (count($notification_list) > 0) {

                        foreach ($notification_list as $notifications) {
                            ?>
                            <li>
                                <i class="fa fa-bell-o" aria-hidden="true"></i>
                                <a href="#"><?php echo $notifications->description; ?></a>
                                <span> 
                                    <?php
                                    $date = new DateTime($notifications->notification_date);
                                    echo ago($date->format('U'));
                                    ?> 
                                </span>
                            </li>
                            <?php
                        }
                    } else {

                        echo "<li><p>You have no notifications yet..</p> </li>";
                    }
                    ?>

                </ul>

            </div>

            <?php
            if (count($notification_list) > 0) {
                ?>
                <div class="pagination_contain">
                    <p>Showing <?php
                        $entry = $limit + $offset;
                        if ($offset == 0) {
                            echo '1';
                        } else {
                            echo $offset + 1;
                        }
                        ?> to <?php
                        if ($entry > $total) {
                            echo $total;
                        } else
                            echo $entry;
                        ?> of <?php echo $total; ?> entries</p>
                    <div class="pagination_main">
                        <?php
                        if (function_exists(custom_pagination)) {
                            custom_pagination($num_of_pages, "", $paged);
                        }
                        ?>
                    </div>
                </div>
            <?php }
            ?>

        </div>

    </div>

</section>

<?php get_footer(); ?>