<?php
/*
 * Template Name: completed_all_tests
 */

get_header();
$current_user = wp_get_current_user();
$user_roles_array = $current_user->roles;
$user_role = array_shift($user_roles_array);
if (!is_user_logged_in() || $user_role != "proofreader") {
    echo '<script>window.location.href="' . get_site_url() . '"</script>';
    exit;
}
$user_id = get_current_user_id();
$test_results = $wpdb->get_results("SELECT *  FROM tbl_proofreader_test WHERE fk_proofreader_id = $user_id GROUP BY test_id ");
$test_count = count($test_results);
$test_completed = get_user_meta($user_id, 'test_completed', TRUE);
if ($test_count != 5 || $test_completed != TRUE) {
    print('<script>window.location.href="' . get_the_permalink(774) . '"</script>');
    exit;
}
?>
<section>
    <div class="breadcum">

        <div class="container">

            <div class="page_title">

                <h1>Completed all tests</h1>

            </div>

        </div>

    </div>

</section>        

<section>

    <div class="container">

        <div class="all_page_proofer">

            <div class="completed_all_test_content">
                
                <h2>You've completed your application</h2>

                <p><br/><br/>Your tests and profile have been sent to our team for review.</p> 

                <p>You will be notified once our team has had a chance to look over your application.</p> 

            </div>

            <div class="btn_blue completed_test">

                <a href="<?php echo get_page_link(768); ?>" class="btn_sky">Go to Profile</a>

            </div>

        </div>

    </div>

</section>


<?php get_footer(); ?>