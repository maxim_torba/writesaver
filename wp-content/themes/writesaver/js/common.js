$(function () {

    //===================================




    //===================================
    var navWidth = $('.navigation').width(); // navigation width
    var listWidth = $('.navigation__list').width(); // navigation__list width

    var botLineWidth = (navWidth - listWidth) / 2;
    //===================================
    $('.navigation__bot-line').css("width", botLineWidth);
    //===================================

    $('.navigation__link').mouseover(function () {

        //==============================
        var elemWidth = $(this).width(); // element width

        var listCoords = $(".navigation__list").offset();
        var listLeftCoords = listCoords.left;

        var linkCoords = $(this).offset();
        var linkLeftCoords = linkCoords.left;

        var linkPos = linkLeftCoords - listLeftCoords;

        var hoverEf = botLineWidth + linkPos + elemWidth;

        //==============================
        $('.navigation__bot-line').css("width", hoverEf);
        $('.navigation__line').css("width", elemWidth);
    });


    //===================================
    // $(window).resize(function() {
    //     $('body').prepend('<div>' + $(window).width() + '</div>');
    // });

    //===================================
    $('.works__slider').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1
    });

    //===================================
    $('.clients__slider').slick({
        slidesToShow: 1,
        arrows: false,
        fade: true

    });
    $('.clients__icon-slider').slick({
        slidesToShow: 4,
        centerPadding: '60px',
        slidesToScroll: 1,
        asNavFor: '.clients__slider',
        focusOnSelect: true,
        autoplay: true,
        responsive: [
            {
                breakpoint: 600,
                settings: {
                    arrows: true,
                    infinite: true,
                    autoplay: true,
                    slidesToScroll: 1,
                    slidesToShow: 3

                }
            }
        ]

    });

    //===================================


        $(window).on('resize', function () {
            var width = $(window).width();
            if (width < 600) {
                console.log('600');

                $('.clients__company-slider').slick({
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    arrows: true,
                    infinite: true,
                    autoplay: true,
                    centerMode: true,
                    centerPadding: '60px',
                    responsive: [
                        {
                            breakpoint: 400,
                            settings: {
                                slidesToScroll: 1,
                                centerPadding: '80px',
                                slidesToShow: 2

                            }
                        }
                    ]
                });

            } else {
                $('.clients__company-slider').slick('destroy');
            }
        });





    //===================================


    //===================================
    $(".proofreading__btn-link").on("click", function (e) {
        e.preventDefault();
        $(".proofreading__content-order").addClass('open');
        $('.proofreading__content').addClass('close');
        $(".proofreading__bot-text").addClass('close');
    });
    $('.proofreading__close-btn').on("click", function (e) {
        e.preventDefault();
        $(".proofreading__content-order").removeClass('open');
        $('.proofreading__content').removeClass('close');
        $(".proofreading__bot-text").removeClass('close');
    });
    //===================================
    $(".burger-btn").on("click", function () {
        $(".nav-wrap").toggleClass('open');
    });
    //===================================


});
// END all function