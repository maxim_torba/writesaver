var socket = require('socket.io');
var express = require('express');
var http = require('http');

var app = express();
var server = http.createServer(app);

var io = socket.listen(server);

var usernames = {};
io.sockets.on('connection', function (socket) {
    console.log("Connected");
// when the client emits 'sendchat', this listens and executes
    socket.on('sendchat', function (data,start,sectionId,docdtlid) {


        // we tell the client to execute 'updatechat' with 2 parameters
        // if (socket.username === 'kirtan' || socket.username === 'vimal')
        //{
        io.sockets.emit('updatechat', socket.connectedUserNames, data,start,sectionId,docdtlid);
        //}
    });

    socket.on('sendDocDtl', function (data, start) {

        // we tell the client to execute 'updatechat' with 2 parameters
        // if (socket.username === 'kirtan' || socket.username === 'vimal')
        //{
        io.sockets.emit('updatechat', socket.connectedUserNames, data);
        //}
    });
    // when the client emits 'adduser', this listens and executes

    socket.on('adduser', function (userarray) {
        console.log("adduser");
        // we store the username in the socket session for this client
        var connectedUserNames = [];
        connectedUserNames.push(userarray);
        socket.connectedUserNames = connectedUserNames;

        // add the client's username to the global list

        connectedUserNames[userarray] = userarray;

        // echo to client they've connected

        //   socket.emit('updatechat', 'SERVER', 'you have connected');

        // echo globally (all clients) that a person has connected

        //    socket.broadcast.emit('updatechat', 'SERVER', username + ' has connected');

        // update the list of users in chat, client-side

        // io.sockets.emit('updateusers', usernames);

    });

    // when the user disconnects.. perform this

    socket.on('disconnect', function () {

        // remove the username from global usernames list

        delete usernames[socket.username];

        // update list of users in chat, client-side

        //  io.sockets.emit('updateusers', usernames);

        // echo globally that this client has left

        //  socket.broadcast.emit('updatechat', 'SERVER', socket.username + ' has disconnected');

    });

});

server.listen(49999);
//server.listen(49152);