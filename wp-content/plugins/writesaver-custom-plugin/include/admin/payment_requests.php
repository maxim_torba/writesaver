<?php
wp_enqueue_style('admin-custom-bootstrap', WRITESAVER_CUSTOM_PLUGIN_URL . '/css/admin/bootstrap.min.css', '', 'all');
wp_enqueue_style('admin-font-style', get_template_directory_uri() . '/css/font-awesome.css', '', '', 'all');
wp_enqueue_style('admin-datatable-style', WRITESAVER_CUSTOM_PLUGIN_URL . '/css/admin/jquery.dataTables.min.css', '', '', 'all');
wp_enqueue_style('admin-custom-style', WRITESAVER_CUSTOM_PLUGIN_URL . '/css/admin/style.css', '', '', 'all');
wp_enqueue_script('admin-custom-bootstrap-js', WRITESAVER_CUSTOM_PLUGIN_URL . '/js/admin/bootstrap.min.js', array('jquery'), '', 'all');
wp_enqueue_script('admin-datatable-script', WRITESAVER_CUSTOM_PLUGIN_URL . '/js/admin/jquery.dataTables.min.js', array('jquery'), '', true);
wp_enqueue_script('admin-jquery-js', WRITESAVER_CUSTOM_PLUGIN_URL . '/js/admin/custom.js', array('jquery'), '', 'all');
?>
<div class="load_overlay" id="loding">
    <img src="<?php echo get_template_directory_uri(); ?>/images/39.gif"/>
</div>
<div class="proof_list" id="proof_list">
    <h1>Payment Requests</h1> 
    <?php
    global $wpdb;
    $paged = ($_REQUEST['paged']) ? $_REQUEST['paged'] : 1;
    $limit = 10;
    $offset = ( $paged - 1 ) * $limit;

    $total_requests = $wpdb->get_var("SELECT COUNT('pk_proofreader_revenue_id')  FROM `tbl_proofreader_revenue`");


    $num_of_pages = ceil($total_requests / $limit);
    $payments = $wpdb->get_results("SELECT * FROM `tbl_proofreader_revenue`  ORDER BY fk_proofreader_id DESC    ");
    $count = $offset + 1;
    ?>
    <table class="table" id="PaymentTable">
        <thead>
            <tr>
                <th></th>
                <th>Proofreader  Name</th>
                <th>Document Name</th>
                <th>Requested date</th>
                <th>Approval date</th>
                <th>Requested amount</th>
                <th>Edited Word</th>
                <th>Paypal ID</th>
                <th>Check Status</th>
                <th>Status</th>                     
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if ($payments) {
                foreach ($payments as $payment) {

                    $proof_info = get_userdata($payment->fk_proofreader_id);

                    $main_docId = $wpdb->get_results("SELECT fk_doc_main_id FROM `wp_customer_document_details`  Where pk_doc_details_id=  $payment->fk_doc_id  AND is_active = 1 LIMIT 1 ");
                    $main_docId = $main_docId[0]->fk_doc_main_id;

                    $main_docName = $wpdb->get_results("SELECT document_title FROM `wp_customer_document_main`  Where pk_document_id =  $main_docId AND Status =1 LIMIT 1 ");
                    $main_docName = $main_docName[0]->document_title;


                    $check_proof = $wpdb->get_row("SELECT * FROM `tbl_proofreaded_doc_details`  Where fk_doc_details_id =  $payment->fk_doc_id");
                    if ($check_proof->fk_proofreader_id == $payment->fk_proofreader_id) {
                        $cstatus = "Single Check";
                    } elseif ($check_proof->Fk_DoubleProofReader_Id == $payment->fk_proofreader_id) {
                        $cstatus = "Double Check";
                    } else {
                        $cstatus = "";
                    }


                    $user_tax = $wpdb->get_results("SELECT * FROM tbl_proofreader_general_details WHERE fk_proofreader_id = $payment->fk_proofreader_id LIMIT 1");
                    ?>
                    <tr>
                        <td><?php echo $count++; ?></td>
                        <td><a class="proofreader_name" href="<?php echo site_url() ?>/wp-admin/admin.php?page=view_user&user=<?php echo $payment->fk_proofreader_id; ?>"><?php echo $proof_info->first_name . ' ' . $proof_info->last_name; ?></a></td>                        
                        <td><a class="proofreader_name" href="<?php echo site_url() ?>/wp-admin/admin.php?page=view_documents&doc_id=<?php echo $main_docId; ?>"><?php echo $main_docName; ?></a></td>
                        <td><?php echo ($payment->requested_date) ? date('m/d/Y h:i:s A', strtotime($payment->requested_date)) : ''; ?></td>
                        <td class="app_date"><?php echo ($payment->approval_date) ? date('m/d/Y h:i:s A', strtotime($payment->approval_date)) : ''; ?></td>
                        <td><?php echo "$" . $payment->requested_amount; ?></td>
                        <td><?php echo $payment->edited_word; ?></td>
                        <td ><?php echo $user_tax[0]->Paypal_ID; ?></td>
                        <td><?php echo $cstatus; ?></td>
                        <td class="payment_status"><?php echo $payment->status; ?></td>

                        <td>
                            <?php if ($payment->status == "Process"): ?>
                                <a href="javascript:void(0);" title="Approve Payment" data-proof_id="<?php echo $payment->fk_proofreader_id; ?>" data-payment_id="<?php echo $payment->pk_proofreader_revenue_id; ?>" class="approve_payment"><i class="fa fa-check" aria-hidden="true"></i></a>                      
                                <?php endif; ?>
                        </td>
                    </tr>
                <?php } ?>
            <?php } ?>
        </tbody>
    </table>
</div>
<script>

    jQuery(document).ready(function () {
        jQuery('#PaymentTable').DataTable({
            "oLanguage": {
                "sEmptyTable": "No payment request available."
            }
        });

        jQuery('.approve_payment').live('click', function (e) {
            e.preventDefault();
            var button = jQuery(this);
            var payment_id = jQuery(this).attr('data-payment_id');
            var proof_id = jQuery(this).attr('data-proof_id');
            if (payment_id) {
                var r = confirm("Are you sure to approve payment?");
                if (r == true) {
                    jQuery('#loding').show();
                    jQuery.ajax({
                        url: "<?php echo admin_url('admin-ajax.php'); ?>",
                        type: "POST",
                        data: {
                            action: 'approve_payment',
                            payment_id: payment_id,
                            proof_id: proof_id
                        },
                        success: function (data) {
                            debugger;
                            jQuery('#loding').hide();
                            if (data == 1) {
                                button.parent('td').prev().text("Completed");
                                var date = new Date();
                                button.parents('tr').find('td.app_date').text("<?php echo date('m/d/Y h:i:s A'); ?>");
                                button.replaceWith('<span class="text-success statusmsg">Approved sucessfully... </span>');
                            } else
                                button.after('<span class="text-danger statusmsg">Not approved sucessfully...</span>');
                            jQuery(".statusmsg").fadeOut(7000);

                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            jQuery('#loding').hide();
                            console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                        }
                    });
                } else {
                    $(r).dialog("close");
                }
            }
            return false;
        });
    });
</script>

