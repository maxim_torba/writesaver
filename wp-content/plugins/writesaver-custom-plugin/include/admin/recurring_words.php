<?php
/*
 *  Writesaver List
 */
wp_enqueue_style('admin-custom-bootstrap', WRITESAVER_CUSTOM_PLUGIN_URL . '/css/admin/bootstrap.min.css', '', 'all');
wp_enqueue_style('admin-font-style', get_template_directory_uri() . '/css/font-awesome.css', '', '', 'all');
wp_enqueue_style('admin-datatable-style', WRITESAVER_CUSTOM_PLUGIN_URL . '/css/admin/jquery.dataTables.min.css', '', '', 'all');
wp_enqueue_style('admin-custom-style', WRITESAVER_CUSTOM_PLUGIN_URL . '/css/admin/style.css', '', '', 'all');
wp_enqueue_script('admin-custom-bootstrap-js', WRITESAVER_CUSTOM_PLUGIN_URL . '/js/admin/bootstrap.min.js', array('jquery'), '', 'all');
wp_enqueue_script('admin-datatable-script', WRITESAVER_CUSTOM_PLUGIN_URL . '/js/admin/jquery.dataTables.min.js', array('jquery'), '', true);
wp_enqueue_script('admin-jquery-js', WRITESAVER_CUSTOM_PLUGIN_URL . '/js/admin/custom.js', array('jquery'), '', 'all');
?>
<div class="load_overlay" id="loding">
    <img src="<?php echo get_template_directory_uri(); ?>/images/39.gif"/>
</div>
<div class="proof_list" id="proof_list">
    <h1>Recurring words</h1> 
    <?php
    global $wpdb;
    $users = $wpdb->get_results("SELECT * FROM `tbl_customer_recurring_words` 
        INNER JOIN `wp_users` ON `tbl_customer_recurring_words`.user_id = `wp_users`.id
        INNER JOIN (SELECT user_id, meta_value as `first_name` FROM `wp_usermeta` WHERE meta_key = 'first_name') as firstname
            ON `wp_users`.id = firstname.user_id
        INNER JOIN (SELECT user_id, meta_value as `last_name` FROM `wp_usermeta` WHERE meta_key = 'last_name') as lastname
            ON `wp_users`.id = lastname.user_id
        ORDER BY `tbl_customer_recurring_words`.id DESC");
    ?>
    <table class="table" id="list_table">
        <thead>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Recurring Words</th>
                <th>Date</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if ($users) {
                foreach ($users as $user) {
                    ?>
                    <tr>
                        <td><a href="<?php echo site_url(); ?>/wp-admin/admin.php?page=view_user&user=<?php echo $user->user_id; ?>" ><?php echo $user->first_name;?></i></a>  
                        </td>                        
                        <td><?php echo $user->last_name; ?></td>
                        <td><?php echo $user->user_email; ?></td>
                        <td><?php echo $user->recurring_words; ?></td>
                        <td><?php echo $user->recurring_date; ?></td>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>
    <?php
    ?>
</div>
<script>
    jQuery(document).ready(function () {
        jQuery('#list_table').DataTable({
            "oLanguage": {
                "sEmptyTable": "No records available."
            }
        });

        jQuery('.status').change(function (event) {

            var button = jQuery(this);
            jQuery('.statusmsg').remove();
            var status = jQuery(this).val();
            var proof_id = button.next('.proof_id').val();
            if (status) {
                var r = confirm("Are you sure to change status to " + status + "?");
                if (r == true) {
                    jQuery('#loding').show();
                    jQuery.ajax({
                        url: "<?php echo admin_url('admin-ajax.php'); ?>",
                        type: "POST",
                        data: {
                            action: 'change_proof_status',
                            proof_id: proof_id,
                            status: status
                        },
                        success: function (data) {
                            debugger;
                            jQuery('#loding').hide();
                            if (data == 1)
                                button.parent('div.test_status').find('.status_msg').html('<span class="text-success statusmsg">Request ' + status + ' sucessfully... </span>');
                            else
                                button.parent('div.test_status').find('.status_msg').html('<span class="text-danger statusmsg">Request not ' + status + ' sucessfully...</span>');
                            jQuery(".statusmsg").fadeOut(5000);

                            if (status == 'rejected' || status == 'accepted') {
                                button.css('pointer-events', 'none');
                                location.reload();
                            }

                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            jQuery('#loding').hide();
                            console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
                        }
                    });
                } else {
                    $(r).dialog("close");
                }
            }
            return false;
        });
    });
</script>

